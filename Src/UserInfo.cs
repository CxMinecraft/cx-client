﻿using System;
using Utils;

namespace CxClient
{
    public class UserInfo
    {
        private UserInfo(string nickname)
        {
            this.Nickname = nickname;
            this.OnlineMode = false;
            this.ClientToken = UUID.GenerateUUID3(nickname);
            this.AccessToken = "";
        }

        private UserInfo(string nickname, UUID client_token = default(UUID), string access_token = null)
        {
            this.Nickname = nickname;
            this.OnlineMode = true;
            this.ClientToken = client_token == default(UUID) ? UUID.GenerateUUID4() : client_token;
            this.AccessToken = access_token;
        }

        public bool OnlineMode
        {
            get;
        }
        public string Nickname
        {
            get;
        }
        /// <summary>
        /// Unique identification of client.<br>
        /// Should be generated once (UUIDv4) and stored.
        /// </summary>
        /// <value>The client token.</value>
        public UUID ClientToken
        {
            get;
        }
        /// <summary>
        /// Used to keep user loged in.
        /// </summary>
        public string AccessToken
        {
            get;
        }

        #region Static

        public static UserInfo CreateOfflinePlayer(string nickname)
        {
            return new UserInfo(nickname);
        }

        /// <summary>
        /// This may take a while...
        /// </summary>
        /// <param name="nickname">Nickname</param>
        /// <param name="password">Password - never store player's password!!!</param>
        public static UserInfo LoginOnlinePlayer(string nickname, string password)
        {
            throw new NotImplementedException();
        }
        public static UserInfo CreateOnlinePlayer(string nickname, UUID client_token, string access_token)
        {
            return new UserInfo(nickname, client_token, access_token);
        }

        #endregion
    }
}