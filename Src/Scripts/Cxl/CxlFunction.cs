﻿using System;
namespace CxClient.Scripts.Cxl
{
    public class CxlFunction
    {
        public CxlFunction(string name)
        {
            this.Name = name;
        }

        public string Name
        {
            get;
        }
    }
}