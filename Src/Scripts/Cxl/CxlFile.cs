﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CxClient.Scripts.Cxl
{
    public class CxlFile
    {
        public CxlFile()
        {
        }

        public List<CxlFunction> Functions
        {
            get;
        } = new List<CxlFunction>();

        public Dictionary<string, string> Variables
        {
            get;
        } = new Dictionary<string, string>();

        public string this[string name]
        {
            get
            {
                string rtn;
                if (Variables.TryGetValue(name, out rtn))
                    return rtn;
                var fun = Functions.FirstOrDefault((f) => f.Name == name);
                if (fun == null)
                    return null;
                return string.Format("Function: {0} (#{1})", fun.Name, fun.GetHashCode());
            }
            set
            {
                AddVariable(name, value);
            }
        }

        public void Remove(string name)
        {
            var fun = Functions.FirstOrDefault((f) => f.Name == name);
            if (fun != null)
                Functions.Remove(fun);

            if (Variables.ContainsKey(name))
                Variables.Remove(name);
        }

        public void AddVariable(string name, string value)
        {
            Remove(name);

            Variables[name] = value;
        }

        public void AddFunction(CxlFunction value)
        {
            Remove(value.Name);

            Functions.Add(value);
        }

        public static CxlFile LoadFromFile(string path)
        {
            if (File.Exists(path))
                return LoadFromStream(File.Open(path, FileMode.Open, FileAccess.Read, FileShare.None));
            else
                return null;
        }

        public static CxlFile LoadFromCode(string code)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(code);
                writer.Flush();
                stream.Position = 0;

                return LoadFromStream(stream);
            }
        }

        public static CxlFile LoadFromStream(Stream stream)
        {
            using (StreamReader reader = new StreamReader(stream))
            {
                //table = {};
                //array = [];
                //string = "";
                //int = 0;
                throw new NotImplementedException();
            }
        }
    }
}