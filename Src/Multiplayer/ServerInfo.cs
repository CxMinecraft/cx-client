﻿using System;
using System.Drawing;

namespace CxClient.Multiplayer
{
    public class ServerInfo
    {
        public ServerInfo(string versionName, int versionProtocol, int playersMax, int playersOnline, string description, Image favIcon = null, PlayerSample[] sample = null, float ping = -1)
        {
            this.VersionName = versionName;
            this.VersionProtocol = versionProtocol;

            this.MaxPlayers = playersMax;
            this.OnlinePlayers = playersOnline;

            this.Description = description;
            this.FavIcon = favIcon;
            this.Sample = sample;

            this.Ping = ping;
        }

        public string VersionName
        {
            get;
        }

        public int VersionProtocol
        {
            get;
        }

        public int MaxPlayers
        {
            get;
        }

        public int OnlinePlayers
        {
            get;
        }

        public string Description
        {
            get;
        }

        public Image FavIcon
        {
            get;
        }

        public PlayerSample[] Sample
        {
            get;
        }

        public float Ping
        {
            get;
        }
    }
}