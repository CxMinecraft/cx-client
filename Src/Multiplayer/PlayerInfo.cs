﻿using System;
using Utils;

namespace CxClient.Multiplayer
{
    public class PlayerInfo
    {
        public PlayerInfo(string username, UUID uuid)
        {
            this.Username = username;
            this.UUID = uuid;
        }

        public string Username
        {
            get;
        }
        public UUID UUID
        {
            get;
        }

        public PlayerEntity Entity
        {
            get;
            set;
        }
    }
}