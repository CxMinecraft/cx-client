﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxClient.Multiplayer.Communication.mc1_7_10.Login.ToServer
{
    public class LoginStart : Packet
    {
        public string Username
        {
            get;
        }

        public LoginStart(string username) : base(0x00, PacketState.Login, PacketSide.ToServer)
        {
            this.Username = username;
        }

        public override string ToString()
        {
            return string.Format("[LoginStart: Username={0}]", Username);
        }

        public override void ToNetwork(BinaryWriter writer)
        {
            PacketEncoding.WriteString(writer, Username);
        }

        public override int SizeInBytes
        {
            get
            {
                return PacketEncoding.CountString(Username);
            }
        }
    }
}