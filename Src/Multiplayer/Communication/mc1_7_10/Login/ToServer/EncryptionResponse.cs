﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxClient.Multiplayer.Communication.mc1_7_10.Login.ToServer
{
    public class EncryptionResponse : Packet
    {
        public byte[] SharedKey
        {
            get;
        }
        public byte[] VerifyToken
        {
            get;
        }

        public EncryptionResponse(byte[] sharedKey, byte[] verifyToken) : base(0x01, PacketState.Login, PacketSide.ToServer)
        {
            this.SharedKey = sharedKey;
            this.VerifyToken = verifyToken;
        }

        public override string ToString()
        {
            return string.Format("[EncryptionResponse: SharedKey={0}, VerifyToken={1}]", SharedKey, VerifyToken);
        }

        public override void ToNetwork(BinaryWriter writer)
        {
            PacketEncoding.WriteByteArray(writer, SharedKey);
            PacketEncoding.WriteByteArray(writer, VerifyToken);
        }

        public override int SizeInBytes
        {
            get
            {
                return PacketEncoding.CountByteArray(SharedKey) + PacketEncoding.CountByteArray(VerifyToken);
            }
        }
    }
}