﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxClient.Multiplayer.Communication.mc1_7_10.Login.ToClient
{
    public class SetCompression : Packet
    {
        public int MaximumSize
        {
            get;
        }

        public SetCompression(int maximumSize) : base(0x03, PacketState.Login, PacketSide.ToClient)
        {
            this.MaximumSize = maximumSize;
        }

        public override string ToString()
        {
            return string.Format("[SetCompression: MaximumSize={0}]", MaximumSize);
        }

        public override void ToNetwork(BinaryWriter writer)
        {
            PacketEncoding.WriteVarInt(writer, MaximumSize < 0 ? -1 : MaximumSize);
        }

        public override int SizeInBytes
        {
            get
            {
                return PacketEncoding.CountVarInt(MaximumSize < 0 ? -1 : MaximumSize);
            }
        }
    }
}