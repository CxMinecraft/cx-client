﻿using System;
using System.IO;
using System.Collections.Generic;
using Utils;

namespace CxClient.Multiplayer.Communication.mc1_7_10.Login.ToClient
{
    public class LoginSuccess : Packet
    {
        public UUID UUID
        {
            get;
        }
        public string Username
        {
            get;
        }

        public LoginSuccess(UUID uuid, string username) : base(0x02, PacketState.Login, PacketSide.ToClient)
        {
            this.UUID = uuid;
            this.Username = username;
        }

        public override string ToString()
        {
            return string.Format("[LoginSuccess: UUID={0}, Username={1}]", UUID, Username);
        }

        public override void ToNetwork(BinaryWriter writer)
        {
            PacketEncoding.WriteString(writer, UUID.ToString(true));
            PacketEncoding.WriteString(writer, Username);
        }

        public override int SizeInBytes
        {
            get
            {
                return PacketEncoding.CountString(UUID.ToString(true)) + PacketEncoding.CountString(Username);
            }
        }
    }
}