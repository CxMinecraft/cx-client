﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxClient.Multiplayer.Communication.mc1_7_10.Login.ToClient
{
    public class EncryptionRequest : Packet
    {
        public string ServerID
        {
            get;
        }
        public byte[] PublicKey
        {
            get;
        }
        public byte[] VerifyToken
        {
            get;
        }

        public EncryptionRequest(string serverID, byte[] publicKey, byte[] verifyToken) : base(0x01, PacketState.Login, PacketSide.ToClient)
        {
            this.ServerID = serverID;
            this.PublicKey = publicKey;
            this.VerifyToken = verifyToken;
        }

        public override string ToString()
        {
            return string.Format("[EncryptionRequest: ServerID={0}, PublicKey={1}, VerifyToken={2}]", ServerID, PublicKey, VerifyToken);
        }

        public override void ToNetwork(BinaryWriter writer)
        {
            PacketEncoding.WriteString(writer, ServerID);

            PacketEncoding.WriteByteArray(writer, PublicKey);
            PacketEncoding.WriteByteArray(writer, VerifyToken);
        }

        public override int SizeInBytes
        {
            get
            {
                int size = 0;

                size += PacketEncoding.CountString(ServerID);
                size += PacketEncoding.CountByteArray(PublicKey);
                size += PacketEncoding.CountByteArray(VerifyToken);

                return size;
            }
        }
    }
}