﻿using System;
using System.IO;
using System.Collections.Generic;
using CxClient.Chat;

namespace CxClient.Multiplayer.Communication.mc1_7_10.Login.ToClient
{
    public class Disconnect : Packet
    {
        public ChatMessage Reason
        {
            get;
        }

        public Disconnect(ChatMessage reason) : base(0x00, PacketState.Login, PacketSide.ToClient)
        {
            this.Reason = reason;
        }

        public Disconnect(string reasonText) : this(new ChatMessagePlain(reasonText))
        {
        }

        public override string ToString()
        {
            return string.Format("[Disconnect: Reason={0}]", Reason);
        }

        public override void ToNetwork(BinaryWriter writer)
        {
            PacketEncoding.WriteString(writer, Reason.ToJSON());
        }

        public override int SizeInBytes
        {
            get
            {
                return PacketEncoding.CountString(Reason.ToJSON());
            }
        }
    }
}