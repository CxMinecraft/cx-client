﻿using System;
using System.IO;

namespace CxClient.Multiplayer.Communication.mc1_7_10
{
    public class UnknownPacket : Packet
    {
        public UnknownPacket() : base(0, (Packet.PacketState)(-1), (PacketSide)(-1))
        {

        }

        public override int SizeInBytes
        {
            get
            {
                return 0;
            }
        }

        public override void ToNetwork(BinaryWriter writer)
        {
        }
    }
}