﻿using System;
using System.Collections.Generic;
using System.IO;
using Utils;

namespace CxClient.Multiplayer.Communication.mc1_7_10
{
    public static class PacketEncoding
    {

        #region VarInt

        public static int ReadVarInt(BinaryReader reader) => VarInt.ReadInt(reader);

        public static int CountVarInt(int value) => VarInt.CountIntBytes(value);

        public static void WriteVarInt(BinaryWriter writer, int value) => VarInt.WriteIntToWriter(value, writer);

        #endregion

        #region VarLong

        public static int ReadVarLong(BinaryReader reader) => VarInt.ReadInt(reader);

        public static int CountVarLong(long value) => VarInt.CountLongBytes(value);

        public static void WriteVarLong(BinaryWriter writer, int value) => VarInt.WriteIntToWriter(value, writer);

        #endregion

        #region String

        public static string ReadString(BinaryReader reader)
        {
            int length = ReadVarInt(reader);
            return System.Text.Encoding.UTF8.GetString(ReadByteArray(reader, length));
        }

        public static int CountString(string value)
        {
            byte[] bytes = value == null ? new byte[0] : System.Text.Encoding.UTF8.GetBytes(value);
            return CountVarInt(bytes.Length) + CountByteArrayWithoutLength(bytes);
        }

        public static void WriteString(BinaryWriter writer, string value)
        {
            byte[] bytes = value == null ? new byte[0] : System.Text.Encoding.UTF8.GetBytes(value);
            WriteVarInt(writer, bytes.Length);
            WriteByteArrayWithoutLength(writer, bytes);
        }

        #endregion

        #region Byte Array

        public static byte[] ReadByteArray(BinaryReader reader) => ReadByteArray(reader, ReadVarInt(reader));

        public static byte[] ReadByteArray(BinaryReader reader, int length)
        {
            byte[] bytes = new byte[length];
            for (int i = 0; i < length; i++)
                bytes[i] = reader.ReadByte();
            return bytes;
        }

        public static int CountByteArray(byte[] values)
        {
            return CountVarInt(values.Length) + CountByteArrayWithoutLength(values);
        }

        public static int CountByteArrayWithoutLength(byte[] values)
        {
            return values.Length;
        }

        public static int CountByteArray(List<byte> values)
        {
            return CountVarInt(values.Count) + CountByteArrayWithoutLength(values);
        }

        public static int CountByteArrayWithoutLength(List<byte> values)
        {
            return values.Count;
        }

        public static void WriteByteArray(BinaryWriter writer, byte[] values)
        {
            PacketEncoding.WriteVarInt(writer, values.Length);
            foreach (var b in values)
                writer.Write(b);
        }

        public static void WriteByteArrayWithoutLength(BinaryWriter writer, byte[] values)
        {
            foreach (var b in values)
                writer.Write(b);
        }

        public static void WriteByteArray(BinaryWriter writer, List<byte> values)
        {
            PacketEncoding.WriteVarInt(writer, values.Count);
            foreach (var b in values)
                writer.Write(b);
        }

        public static void WriteByteArrayWithoutLength(BinaryWriter writer, List<byte> values)
        {
            foreach (var b in values)
                writer.Write(b);
        }

        #endregion

        #region Boolean

        public static bool ReadBoolean(BinaryReader reader)
        {
            return reader.ReadByte() != 0x00;
        }

        public static int CountBoolean()
        {
            return 1;
        }
        public static int CountBoolean(bool value)
        {
            return 1;
        }

        public static void WriteBoolean(BinaryWriter writer, bool value)
        {
            writer.Write((byte)(value ? 0x01 : 0x00));
        }

        #endregion

        #region Byte

        public static sbyte ReadByte(BinaryReader reader)
        {
            return (sbyte)(((int)reader.ReadByte()) - 128);
        }

        public static int CountByte() => 1;
        public static int CountByte(sbyte value) => 1;

        public static void WriteByte(BinaryWriter writer, sbyte value)
        {
            writer.Write(BitConverter.GetBytes(value)[0]);
        }

        #endregion

        #region Unsigned Byte

        public static byte ReadUnsignedByte(BinaryReader reader) => reader.ReadByte();

        public static int CountUnsignedByte() => 1;
        public static int CountUnsignedByte(sbyte value) => 1;

        public static void WriteUnsignedByte(BinaryWriter writer, byte value) => writer.Write(value);

        #endregion

        #region Short

        public static short ReadShort(BinaryReader reader)
        {
            byte[] bytes = ReadByteArray(reader, 2);
            Array.Reverse(bytes);
            return BitConverter.ToInt16(bytes, 0);
        }

        public static int CountShort() => 2;
        public static int CountShort(short value) => 2;

        public static void WriteShort(BinaryWriter writer, short value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            Array.Reverse(bytes);
            WriteByteArrayWithoutLength(writer, bytes);
        }

        #endregion

        #region Unsigned Short

        public static ushort ReadUnsignedShort(BinaryReader reader)
        {
            byte[] bytes = ReadByteArray(reader, 2);
            Array.Reverse(bytes);
            return BitConverter.ToUInt16(bytes, 0);
        }

        public static int CountUnsignedShort() => 2;
        public static int CountUnsignedShort(ushort value) => 2;

        public static void WriteUnsignedShort(BinaryWriter writer, ushort value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            Array.Reverse(bytes);
            WriteByteArrayWithoutLength(writer, bytes);
        }

        #endregion

        #region Int

        public static int ReadInt(BinaryReader reader)
        {
            byte[] bytes = ReadByteArray(reader, 4);
            Array.Reverse(bytes);
            return BitConverter.ToInt32(bytes, 0);
        }

        public static int CountInt() => 4;
        public static int CountInt(int value) => 4;

        public static void WriteInt(BinaryWriter writer, int value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            Array.Reverse(bytes);
            WriteByteArrayWithoutLength(writer, bytes);
        }

        #endregion

        #region Long

        public static long ReadLong(BinaryReader reader)
        {
            byte[] bytes = ReadByteArray(reader, 8);
            Array.Reverse(bytes);
            return BitConverter.ToInt64(bytes, 0);
        }

        public static int CountLong() => 8;
        public static int CountLong(long value) => 8;

        public static void WriteLong(BinaryWriter writer, long value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            Array.Reverse(bytes);
            WriteByteArrayWithoutLength(writer, bytes);
        }

        #endregion

        #region Float

        public static float ReadFloat(BinaryReader reader)
        {
            byte[] bytes = ReadByteArray(reader, 4);
            Array.Reverse(bytes);
            return BitConverter.ToSingle(bytes, 0);
        }

        public static int CountFloat() => 4;
        public static int CountFloat(float value) => 4;

        public static void WriteFloat(BinaryWriter writer, float value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            Array.Reverse(bytes);
            WriteByteArrayWithoutLength(writer, bytes);
        }

        #endregion

        #region Double

        public static double ReadDouble(BinaryReader reader)
        {
            byte[] bytes = ReadByteArray(reader, 8);
            Array.Reverse(bytes);
            return BitConverter.ToDouble(bytes, 0);
        }

        public static int CountDouble() => 8;
        public static int CountDouble(double value) => 8;

        public static void WriteDouble(BinaryWriter writer, double value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            Array.Reverse(bytes);
            WriteByteArrayWithoutLength(writer, bytes);
        }

        #endregion

        #region UUID

        public static UUID ReadUUID(BinaryReader reader)
        {
            return new UUID(ReadByteArray(reader, 16));
        }

        public static void WriteUUID(BinaryWriter writer, UUID value)
        {
            WriteByteArrayWithoutLength(writer, value.ByteArray);
        }

        #endregion

        #region Angle

        /// <summary>
        /// 0 - 255
        /// </summary>
        public static byte ReadAngleByte(BinaryReader reader) => reader.ReadByte();

        /// <summary>
        /// 0.0 - 1.0
        /// </summary>
        public static float ReadAngleFloat(BinaryReader reader)
        {
            return ReadAngleByte(reader) / 256f;
        }

        public static int CountAngle() => 1;
        public static int CountAngle(float value) => 1;

        public static void WriteAngleFloat(BinaryWriter writer, float value)
        {
            value %= 1;
            writer.Write((byte)(value * 256));
        }

        #endregion

        #region Velocity

        public static double ReadVelocity(BinaryReader reader) => ReadShort(reader) / 8000d;

        public static int CountVelocity() => CountShort();
        public static int CountVelocity(double velocity) => CountShort();

        public static void WriteVelocity(BinaryWriter writer, double velocity) => WriteShort(writer, (short)(velocity * 8000));

        #endregion

    }
}