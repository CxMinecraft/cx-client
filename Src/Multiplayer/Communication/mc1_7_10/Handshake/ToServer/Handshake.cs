﻿using System;
using System.Collections.Generic;
using System.IO;

namespace CxClient.Multiplayer.Communication.mc1_7_10.Handshake.ToServer
{
    public class Handshake : Packet
    {
        public int ProtocolVersion
        {
            get;
        }

        public string ServerAddress
        {
            get;
        }

        public ushort ServerPort
        {
            get;
        }

        public Handshake.NextState Next
        {
            get;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CxClient.Multiplayer.Communication.mc1_7_10.Handshake.ToServer.Handshake"/> class.
        /// </summary>
        /// <param name="protocolVersion">VarInt</param>
        /// <param name="serverAddress">String</param>
        /// <param name="serverPort">Unsigned Short</param>
        /// <param name="nextState">VarInt</param>
        public Handshake(int protocolVersion, string serverAddress, ushort serverPort, NextState nextState) : base(0x00, PacketState.HandShaking, PacketSide.ToServer)
        {
            this.ProtocolVersion = protocolVersion;
            this.ServerAddress = serverAddress;
            this.ServerPort = serverPort;
            this.Next = nextState;
        }

        public Handshake(ProtocolVersion version, string serverAddress, ushort serverPort, NextState nextState) : this((int)version, serverAddress, serverPort, nextState)
        {
        }

        public Handshake(int version, System.Net.IPAddress serverAddress, ushort serverPort, NextState nextState) : this(version, serverAddress.ToString(), serverPort, nextState)
        {
        }

        public Handshake(ProtocolVersion version, System.Net.IPAddress serverAddress, ushort serverPort, NextState nextState) : this((int)version, serverAddress.ToString(), serverPort, nextState)
        {
        }

        public enum NextState
        {
            Status = 1,
            Login = 2
        }

        public override string ToString()
        {
            return string.Format("[Handshake: ProtocolVersion={0}, ServerAddress={1}, ServerPort={2}, Next={3}]", ProtocolVersion, ServerAddress, ServerPort, Next);
        }

        public override void ToNetwork(BinaryWriter writer)
        {
            PacketEncoding.WriteVarInt(writer, ProtocolVersion);
            PacketEncoding.WriteString(writer, ServerAddress);
            PacketEncoding.WriteUnsignedShort(writer, ServerPort);
            PacketEncoding.WriteVarInt(writer, (int)Next);
        }
        public override int SizeInBytes
        {
            get
            {
                int size = 0;
                size += PacketEncoding.CountVarInt(ProtocolVersion);
                size += PacketEncoding.CountString(ServerAddress);
                size += PacketEncoding.CountUnsignedShort(ServerPort);
                size += PacketEncoding.CountVarInt((int)Next);
                return size;
            }
        }
    }
}