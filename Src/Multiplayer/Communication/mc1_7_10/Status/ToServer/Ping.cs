﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxClient.Multiplayer.Communication.mc1_7_10.Status.ToServer
{
    public class Ping : Packet
    {
        public long PingID
        {
            get;
        }

        public Ping(long id) : base(0x01, PacketState.Status, PacketSide.ToServer)
        {
            this.PingID = id;
        }

        public override string ToString()
        {
            return string.Format("[Ping: PingID={0}]", PingID);
        }

        public override void ToNetwork(BinaryWriter writer)
        {
            PacketEncoding.WriteLong(writer, PingID);
        }

        public override int SizeInBytes
        {
            get
            {
                return PacketEncoding.CountLong();
            }
        }
    }
}