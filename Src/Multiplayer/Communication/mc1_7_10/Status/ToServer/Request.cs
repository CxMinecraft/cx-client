﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxClient.Multiplayer.Communication.mc1_7_10.Status.ToServer
{
    public class Request : Packet
    {
        public Request() : base(0x00, PacketState.Status, PacketSide.ToServer)
        {
        }

        public override string ToString()
        {
            return "[Request]";
        }

        public override void ToNetwork(BinaryWriter writer)
        {
        }

        public override int SizeInBytes
        {
            get
            {
                return 0;
            }
        }
    }
}