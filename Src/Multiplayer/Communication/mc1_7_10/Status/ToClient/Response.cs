﻿using System;
using System.IO;
using System.Collections.Generic;
using Utils.Configurations;
using Utils;
using Utils.Extensions;

namespace CxClient.Multiplayer.Communication.mc1_7_10.Status.ToClient
{
    public class Response : Packet
    {

        #region Variables

        public string ResponseJSON
        {
            get;
        }

        public Configuration ResponseConfiguration
        {
            get;
        }

        #endregion

        #region Constructors

        public Response(string response) : base(0x00, PacketState.Status, PacketSide.ToClient)
        {
            this.ResponseJSON = response;
            try
            {
                this.ResponseConfiguration = JSON.Load(response);
            }
            catch
            {
                this.ResponseConfiguration = null;
            }
        }

        public Response(string versionName, int versionProtocol, int playersMax, int playersOnline, PlayerSample[] playersSample, string description, System.Drawing.Image iconPNG) : base(0x00, PacketState.Status, PacketSide.ToClient)
        {
            Configuration cfg = new Configuration();
            cfg.Set("version.name", versionName);
            cfg.Set("version.protocol", versionProtocol);

            cfg.Set("players.max", playersMax);
            cfg.Set("players.online", playersOnline);

            if (playersSample != null)
            {
                List<Configuration> cfg_list = new List<Configuration>(playersSample.Length);
                for (int i = 0; i < playersSample.Length; i++)
                {
                    Configuration c = new Configuration();
                    PlayerSample ps = playersSample[i];
                    c.Set("name", ps.Name);
                    c.Set("id", ps.UUID.ToString(true));
                    cfg_list.Add(c);
                }
                cfg.Set("players.sample", cfg_list);
            }

            //TODO change to Chat
            cfg.Set("description.text", description);

            if (iconPNG != null)
                cfg.Set("favicon", ImageUtils.EncodeBase64WithPrefix(iconPNG));

            this.ResponseConfiguration = cfg;
            this.ResponseJSON = cfg.SaveAsJSON();
        }

        #endregion

        public string VersionName
        {
            get
            {
                return ResponseConfiguration.GetString("version.name");
            }
        }

        public int VersionProtocol
        {
            get
            {
                return ResponseConfiguration.GetInteger("version.protocol");
            }
        }

        public int MaxPlayers
        {
            get
            {
                return ResponseConfiguration.GetInteger("players.max");
            }
        }

        public int OnlinePlayers
        {
            get
            {
                return ResponseConfiguration.GetInteger("players.online");
            }
        }

        public string Description
        {
            get
            {
                //TODO change to Chat
                return ResponseConfiguration.GetString("description.text");
            }
        }

        public System.Drawing.Image FavIcon
        {
            get
            {
                return ImageUtils.DecodeBase64WithPrefix(ResponseConfiguration.GetString("favicon"));
            }
        }

        public PlayerSample[] Sample
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override string ToString()
        {
            return string.Format("[Status: Response={0}]", ResponseJSON.Replace("\n", ""));
        }

        public override void ToNetwork(BinaryWriter writer)
        {
            PacketEncoding.WriteString(writer, ResponseJSON);
        }

        public ServerInfo ToServerInfo(float ping = -1)
        {
            return new ServerInfo(VersionName, VersionProtocol, MaxPlayers, OnlinePlayers, Description, FavIcon, Sample, ping);
        }

        public override int SizeInBytes
        {
            get
            {
                return PacketEncoding.CountString(ResponseJSON);
            }
        }
    }
}