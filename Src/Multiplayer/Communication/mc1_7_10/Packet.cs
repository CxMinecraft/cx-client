﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Utils;
using Utils.Extensions;

namespace CxClient.Multiplayer.Communication.mc1_7_10
{
    public abstract class Packet : IPacket
    {
        public int ID
        {
            get;
        }

        public PacketState State
        {
            get;
        }

        public PacketSide Side
        {
            get;
        }

        protected Packet(int id, PacketState state, PacketSide side)
        {
            this.ID = id;
            this.State = state;
            this.Side = side;
        }

        public abstract void ToNetwork(BinaryWriter writer);

        public abstract int SizeInBytes
        {
            get;
        }

        #region Enums

        public enum PacketState
        {
            HandShaking,
            Play,
            Status,
            Login
        }

        #endregion

        #region Static

        #region Protocol versions

        public const int Protocol_1_7_10 = 5;
        public const int Protocol_1_8 = 47;
        public const int Protocol_1_9_4 = 110;
        public const int Protocol_1_10 = 210;
        public const int Protocol_1_11_2 = 316;

        #endregion

        /// <summary>
        /// Register parsers
        /// </summary>
        static Packet()
        {
            //Handshake
            {
                //To Server
                {
                    Packet.RegisterParser(0x00, PacketState.HandShaking, PacketSide.ToServer, (id, reader, length) =>
                        new Handshake.ToServer.Handshake(
                        PacketEncoding.ReadVarInt(reader),
                        PacketEncoding.ReadString(reader),
                        PacketEncoding.ReadUnsignedShort(reader),
                        (Handshake.ToServer.Handshake.NextState)PacketEncoding.ReadVarInt(reader)
                    )
                    );
                }
            }
            //Status
            {
                //To Client
                {
                }
                //To Server
                {
                }
            }
            //Login
            {
                //To Client
                {
                }
                //To Server
                {
                }
            }
        }

        /// <summary>
        /// Send packet to client.
        /// </summary>
        /// <param name="packet">The packet.</param>
        public static void SendPacket(BinaryWriter writer, Packet packet, bool compressionEnabled = false, int compressionThreshold = 32)
        {
            if (writer == null || packet == null)
                return;

            byte[] id_bytes = VarInt.WriteIntToByteArray(packet.ID);
            int length = id_bytes.Length + packet.SizeInBytes;

            if (compressionEnabled)
            {
                int uncompressedLength = packet.SizeInBytes + id_bytes.Length;

                if (uncompressedLength >= compressionThreshold)//compress
                {
                    var compress_id = CompressionUtils.Compress(id_bytes);
                    using (var compressed_writer = writer.Compressed())
                    {
                        packet.ToNetwork(compressed_writer);

                        //total length
                        VarInt.WriteIntToWriter(compress_id.Length + ((int)compressed_writer.BaseStream.Position) + VarInt.CountIntBytes(uncompressedLength), writer);
                        //uncompressed length
                        VarInt.WriteIntToWriter(uncompressedLength, writer);
                        //packet id
                        writer.Write(compress_id);
                        //packet content
                        compressed_writer.BaseStream.Position = 0;
                        compressed_writer.BaseStream.CopyTo(writer.BaseStream);
                    }
                }
                else//do not compress
                {
                    //packet length
                    VarInt.WriteIntToWriter(length + 1, writer);
                    //compressed data length
                    VarInt.WriteIntToWriter(0, writer);
                    //packet id
                    writer.Write(id_bytes);
                    //packet content
                    packet.ToNetwork(writer);
                    writer.Flush();
                }
            }
            else
            {
                //packet length
                VarInt.WriteIntToWriter(length, writer);
                //packet id
                writer.Write(id_bytes);
                //packet content
                packet.ToNetwork(writer);
                writer.Flush();
            }
        }

        #region Parsing

        public static Packet ParsePacket(PacketState state, PacketSide side, BinaryReader reader)
        {
            int length = VarInt.ReadInt(reader);

            int id = VarInt.ReadInt(reader);

            ParserDelegate parser;
            if (!_parsers.TryGetValue(new Tuple<int, PacketState, PacketSide>(id, state, side), out parser) || parser == null)
                return null;
            return parser(id, reader, length);
        }

        public delegate Packet ParserDelegate(int id, BinaryReader reader, int length);

        private static readonly Dictionary<Tuple<int, PacketState, PacketSide>, ParserDelegate> _parsers = new Dictionary<Tuple<int, PacketState, PacketSide>, ParserDelegate>();

        public static bool RegisterParser(int id, PacketState state, PacketSide side, ParserDelegate parser)
        {
            if (id < 0)
            {
                Console.Error.WriteLine("Failed to register parser - invalid ID '{0}'", id);
                return false;
            }
            if (!Enum.IsDefined(typeof(PacketState), state))
            {
                Console.Error.WriteLine("Failed to register parser - invalid packet state '{0}'", state);
                return false;
            }
            if (!Enum.IsDefined(typeof(PacketSide), side))
            {
                Console.Error.WriteLine("Failed to register parser - invalid packet side '{0}'", state);
                return false;
            }

            Tuple<int, PacketState, PacketSide> tuple = new Tuple<int, PacketState, PacketSide>(id, state, side);

            if (_parsers.ContainsKey(tuple))
            {
                Console.Error.WriteLine("Failed to register parser - ID is already registred ({0})", id);
                return true;
            }

            _parsers.Add(tuple, parser);
            return true;
        }

        #endregion

        #endregion
    }
}