﻿using System;
namespace CxClient.Multiplayer.Communication
{
    public enum PacketSide
    {
        ToClient = 0,
        ToServer = 1
    }
}