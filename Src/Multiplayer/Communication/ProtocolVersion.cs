﻿using System;
namespace CxClient.Multiplayer.Communication
{
    public enum ProtocolVersion
    {
        /// <summary>
        /// Minecraft 1.7.6<br>
        /// Minecraft 1.7.7<br>
        /// Minecraft 1.7.8<br>
        /// Minecraft 1.7.9<br>
        /// Minecraft 1.7.10
        /// </summary>
        Minecraft_1_7_10 = 5,
        /// <summary>
        /// Minecraft 1.8<br>
        /// Minecraft 1.8.1<br>
        /// Minecraft 1.8.2<br>
        /// Minecraft 1.8.3<br>
        /// Minecraft 1.8.4<br>
        /// Minecraft 1.8.5<br>
        /// Minecraft 1.8.6<br>
        /// Minecraft 1.8.7<br>
        /// Minecraft 1.8.8<br>
        /// Minecraft 1.8.9
        /// </summary>
        Minecraft_1_8_9 = 47,
        /// <summary>
        /// Minecraft 1.9.3<br>
        /// Minecraft 1.9.4
        /// </summary>
        Minecraft_1_9_4 = 110,
        /// <summary>
        /// Minecraft 1.10<br>
        /// Minecraft 1.10.1<br>
        /// Minecraft 1.10.2
        /// </summary>
        Minecraft_1_10_2 = 210,
        /// <summary>
        /// Minecraft 1.11
        /// </summary>
        Minecraft_1_11 = 315,
        /// <summary>
        /// Minecraft 1.11.1<br>
        /// Minecraft 1.11.2
        /// </summary>
        Minecraft_1_11_2 = 316
    }
}