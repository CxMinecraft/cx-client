﻿using System;
using System.IO;

namespace CxClient.Multiplayer.Communication
{
    public interface IPacket
    {

        void ToNetwork(BinaryWriter writer);

        int SizeInBytes
        {
            get;
        }
    }
}