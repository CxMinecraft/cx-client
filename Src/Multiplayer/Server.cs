﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using CxClient.Multiplayer.Communication;

namespace CxClient.Multiplayer
{
    public static class Server
    {
        /// <summary>
        /// Wait time in milliseconds when not connected to server
        /// </summary>
        private const int NotConnectedWaitTime = 20;

        static Server()
        {
            TcpWriteThread = new Thread(() =>
            {
                while (true)
                {
                    try
                    {
                        while (true)
                        {
                            if (!Tcp.Connected)
                            {
                                Thread.Sleep(NotConnectedWaitTime);
                                continue;
                            }

                            IPacket packet = null;
                            lock (_packetsToSend)
                            {
                                if (_packetsToSend.Any())
                                    packet = _packetsToSend.Dequeue();
                                else
                                    continue;
                            }

                            if (PacketSend != null)
                                PacketSend(ref packet);

                            if (packet == null)
                                continue;

                            if (packet is Communication.mc1_7_10.Packet)
                            {
                                var p = (Communication.mc1_7_10.Packet)packet;

                                //TODO add packet logger

                                Communication.mc1_7_10.Packet.SendPacket(_writer, p, CompressionEnabled, CompressionThreshold);
                            }
                            //TODO add more protocol versions
                            else
                            {
                                Console.Error.WriteLine("Unknown packet '{0}'", packet);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.Error.WriteLine("Packet sending exception\n{0}", e);
                    }
                }
            });
            TcpWriteThread.Start();

            AppDomain.CurrentDomain.ProcessExit += (sender, e) =>
            {
                Disconnect();
            };
        }

        private static Thread TcpWriteThread;

        private static TcpClient Tcp = new TcpClient();
        private static BinaryReader _reader;
        private static BinaryWriter _writer;
        public static bool CompressionEnabled
        {
            get
            {
                return CompressionThreshold >= 0;
            }
        }
        public static int CompressionThreshold = -1;

        public static ProtocolVersion Version = ProtocolVersion.Minecraft_1_7_10;

        public static int CurrentCommunicationState = 0;

        public static IPAddress Address
        {
            get;
            private set;
        }
        public static int Port
        {
            get;
            private set;
        }

        public const int DefaultPort = 25565;
        public const ProtocolVersion DefaultVersion = ProtocolVersion.Minecraft_1_7_10;

        #region Connection

        public static readonly string ServerAddressRegex = "^([a-zA-Z0-9_-]+(\\.){0,1})+((:){1}[0-9]+){0,1}$";
        public static readonly string ServerAddressWithoutPortRegex = "^([a-zA-Z0-9_-]+(\\.){0,1})+$";

        /// <summary>
        /// Connect to server using IPv4 address
        /// </summary>
        /// <param name="address">IPv4 address (does not work with IPv6).</param>
        public static bool Connect(UserInfo userInfo, string address, ProtocolVersion version = DefaultVersion)
        {
            if (string.IsNullOrWhiteSpace(address))
                return false;

            address = address.Trim();

            if (Regex.IsMatch(address, ServerAddressWithoutPortRegex))
            {
                IPAddress ip;
                if (IPAddress.TryParse(address, out ip))
                    return Connect(userInfo, ip, version: version);
                else
                    return false;
            }
            else if (Regex.IsMatch(address, ServerAddressWithoutPortRegex))
            {
                int index = address.IndexOf(':');
                IPAddress ip;
                if (IPAddress.TryParse(address.Substring(0, index), out ip))
                {
                    int port;
                    if (int.TryParse(address.Substring(index + 1), out port))
                        return Connect(userInfo, ip, port, version);
                    else
                        return false;
                }
                else
                    return false;
            }
            else
                return false;
        }

        public static PlayerInfo Player = null;

        public static bool Connect(UserInfo userInfo, IPAddress address, int port = DefaultPort, ProtocolVersion version = DefaultVersion)
        {
            if (!Tcp.Connected)
                return false;

            lock (Tcp)
            {
                try
                {
                    Tcp.Connect(address, port);
                    var stream = Tcp.GetStream();
                    _writer = new BinaryWriter(stream);
                    _reader = new BinaryReader(stream);
                }
                catch
                {
                    CurrentState = State.Disconnected;
                    Tcp.Close();
                    return false;
                }

                Address = address;
                Port = port;
                Version = version;

                CurrentState = State.Connecting;

                //TODO send packets
                //Connection packets
                switch (version)
                {
                    case ProtocolVersion.Minecraft_1_7_10:
                        {
                            SendPacket(new Communication.mc1_7_10.Handshake.ToServer.Handshake(ProtocolVersion.Minecraft_1_7_10, Address, (ushort)Port, Communication.mc1_7_10.Handshake.ToServer.Handshake.NextState.Login));
                            SendPacket(new Communication.mc1_7_10.Login.ToServer.LoginStart(userInfo.Nickname));

                            IPacket packet = ReadPacket();
                            while (true)
                            {
                                if (packet is Communication.mc1_7_10.Login.ToClient.EncryptionRequest)
                                {
                                    var p = (Communication.mc1_7_10.Login.ToClient.EncryptionRequest)packet;
                                    throw new NotImplementedException();//TODO implement encryption
                                }
                                else if (packet is Communication.mc1_7_10.Login.ToClient.LoginSuccess)
                                {
                                    var p = (Communication.mc1_7_10.Login.ToClient.LoginSuccess)packet;
                                    Player = new PlayerInfo(p.Username, p.UUID);
                                    break;
                                }
                                else if (packet is Communication.mc1_7_10.Login.ToClient.SetCompression)
                                {
                                    var p = (Communication.mc1_7_10.Login.ToClient.SetCompression)packet;
                                    CompressionThreshold = p.MaximumSize;
                                }
                                else
                                {
                                    Console.Error.WriteLine("Unknown packet received during Login state from server with address '{0}' and port '{1}' - '{2}'", Address, Port, packet);
                                    if (Settings.ClientSettings.FailOnUnknownPacket)
                                    {
                                        CurrentState = State.Disconnected;
                                        Tcp.Close();
                                        return false;
                                    }
                                }
                            }

                            if (Player == null)
                            {
                                Console.Error.WriteLine("No LoginSuccess packet received during Login state from server with address '{0}' and port '{1}'", Address, Port);

                                CurrentState = State.Disconnected;
                                Tcp.Close();
                                return false;
                            }
                        }
                        break;
                    default:
                        Console.Error.WriteLine("Unknown ProtocolVersion of server with address '{0}' and port '{1}' - version: {2}", Address, Port, version);
                        CurrentState = State.Disconnected;
                        Tcp.Close();
                        return false;
                }

                CurrentState = State.Connected;
                Connected = true;

                if (ConnectedToServer != null)
                    ConnectedToServer(Address, Port);

                return true;
            }
        }

        public enum State
        {
            Disconnected = 0,
            Connecting,
            Connected
        }

        public static State CurrentState
        {
            get;
            private set;
        } = State.Disconnected;

        public static bool Connected
        {
            get;
            private set;
        }

        public static void Disconnect()
        {
            Tcp.Close();
            _writer.Dispose();
            _reader.Dispose();

            if (DisconnectedFromServer != null)
                DisconnectedFromServer(Address, Port);
        }

        #endregion

        public static IPacket ReadPacket()
        {
            if (!Tcp.Connected)
                return null;

            switch (Version)
            {
                case ProtocolVersion.Minecraft_1_7_10:
                    {
                        IPacket packet = Communication.mc1_7_10.Packet.ParsePacket((Communication.mc1_7_10.Packet.PacketState)CurrentCommunicationState, Communication.PacketSide.ToServer, _reader);

                        if (packet == null)
                        {
                            Console.Error.WriteLine("Unsupported packet received");
                            return null;
                        }

                        if (packet is Communication.mc1_7_10.UnknownPacket)
                        {
                            Console.Error.WriteLine("Unsupported packet received:\nState: {0}\nID: {1}", CurrentCommunicationState, (Communication.mc1_7_10.UnknownPacket)packet);
                            return null;
                        }

                        //TODO add packet logger

                        if (PacketReceived != null)
                            PacketReceived(ref packet);

                        return packet;
                    }
                //TODO add more protocol versions
                default:
                    Console.Error.WriteLine("Unsupported protocol version '{0}'", Version);
                    return null;
            }
        }

        private static Queue<IPacket> _packetsToSend = new Queue<IPacket>();

        public static void SendPacket(IPacket packet)
        {
            lock (_packetsToSend)
            {
                _packetsToSend.Enqueue(packet);
            }
        }


        #region Events

        public delegate void PacketDelegate(ref IPacket packet);

        public static event PacketDelegate PacketSend;
        public static event PacketDelegate PacketReceived;

        public delegate void ServerDelegate(IPAddress address, int port);

        public static event ServerDelegate ConnectedToServer;
        public static event ServerDelegate DisconnectedFromServer;

        #endregion

        #region Server info

        public static async Task<ServerInfo> RetrieveServerInfo(IPAddress address, int port)
        {
            using (TcpClient client = new TcpClient())
            {
                await client.ConnectAsync(address, port);
                var stream = client.GetStream();
                var writer = new BinaryWriter(stream);
                var reader = new BinaryReader(stream);

                //Handshake
                Communication.mc1_7_10.Packet.SendPacket(writer, new Communication.mc1_7_10.Handshake.ToServer.Handshake(Communication.mc1_7_10.Packet.Protocol_1_7_10, address.ToString(), (ushort)port, Communication.mc1_7_10.Handshake.ToServer.Handshake.NextState.Status));
                //Request
                Communication.mc1_7_10.Packet.SendPacket(writer, new Communication.mc1_7_10.Status.ToServer.Request());

                //Response
                var packet_response = Communication.mc1_7_10.Packet.ParsePacket(Communication.mc1_7_10.Packet.PacketState.Status, Communication.PacketSide.ToClient, reader);
                var response = (Communication.mc1_7_10.Status.ToClient.Response)packet_response;


                {
                    DateTime time_start = DateTime.UtcNow;

                    //Ping
                    long ping_id = Utils.StaticRandom.NextLong();
                    Communication.mc1_7_10.Packet.SendPacket(writer, new Communication.mc1_7_10.Status.ToServer.Ping(ping_id));

                    //Pong
                    var packet_pong = Communication.mc1_7_10.Packet.ParsePacket(Communication.mc1_7_10.Packet.PacketState.Status, Communication.PacketSide.ToClient, reader);
                    var pong = (Communication.mc1_7_10.Status.ToClient.Pong)packet_pong;

                    DateTime time_end = DateTime.UtcNow;

                    var ping_time = time_end - time_start;
                    if (ping_id == pong.PingID)
                        return response.ToServerInfo((float)ping_time.TotalMilliseconds);
                    else
                        return response.ToServerInfo();
                }
            }
        }

        #endregion
    }
}