﻿using System;
namespace CxClient
{
    public interface IMenuBackground
    {
        void Update(double delta);
        void Render();
    }
}