﻿using System;

namespace CxClient.NBT
{
    public class NBT_End : NBT_Tag
    {

        #region Constructors

        public NBT_End() : base(null, 0)
        {
        }

        #endregion

        public override void Write(System.IO.BinaryWriter writer, bool onlyValue = false)
        {
            if (onlyValue)
                return;

            writer.Write(ID);
        }

        public override string ToString()
        {
            return "NBT_End";
        }

        public override NBT_Tag Clone()
        {
            return new NBT_End();
        }
    }
}