﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Collections;
using System.Linq;

namespace CxClient.NBT
{
    public class NBT_Compound : NBT_Tag, IEnumerable<NBT_Tag>
    {
        public NBT_Compound(string name, params NBT_Tag[] tags) : base(name, 10)
        {
            this.Tags = new List<NBT_Tag>(tags);
        }

        public List<NBT_Tag> Tags
        {
            get;
        }

        private static NBT_End End = new NBT_End();

        public override void Write(BinaryWriter writer, bool onlyValue = false)
        {
            base.Write(writer, onlyValue);

            foreach (var tag in Tags)
                if (tag != null)
                    tag.Write(writer);

            End.Write(writer);
        }

        public override string ToString()
        {
            return string.Format("NBT_Compound({0}): {1} entries", Name, Count);
        }

        #region Saving

        public void Save(string path)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(path, FileMode.CreateNew)))
            {
                this.Write(writer);
                writer.Flush();
            }
        }

        #endregion

        #region Loading

        public static NBT_Compound Load(string path) => CompoundLoader.LoadFromFile(path);

        #endregion

        #region foreach

        public IEnumerator<NBT_Tag> GetEnumerator()
        {
            return this.Tags.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Indexes

        public NBT_Tag this[int index]
        {
            get
            {
                return Tags[index];
            }
        }

        public int Count
        {
            get
            {
                return Tags.Count;
            }
        }

        #endregion

        public override NBT_Tag Clone()
        {
            var compound = new NBT_Compound(Name);

            foreach (var nbt in this.Tags)
                compound.Add(nbt.Clone());

            return compound;
        }

        public bool ContainsName(string name) => Tags.Any((nbt) => nbt.Name == name);

        #region Operators

        public static NBT_Compound operator +(NBT_Compound nbt_1, NBT_Compound nbt_2)
        {
            NBT_Compound nbt = (NBT_Compound)nbt_2.Clone();

            foreach (var n in nbt_1.Tags)
                if (!nbt.ContainsName(n.Name))
                    nbt.Add(n);

            return nbt;
        }

        #endregion
    }
}