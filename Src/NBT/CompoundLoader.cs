﻿using System;
using System.IO;
using System.Text;
using System.Reflection;
using Utils.Extensions;

namespace CxClient.NBT
{
    public static class CompoundLoader
    {
        /// <summary>
        /// Load Compound from file.<br>
        /// <see cref="CxClient.NBT.NBT_List{}"/> is another list is not supported (yet?).
        /// </summary>
        /// <param name="path">Path.</param>
        public static NBT_Compound LoadFromFile(string path)
        {
            if (!File.Exists(path))
                return null;
            using (BinaryReader reader = new BinaryReader(File.OpenRead(path)))
            {
                if (ReadUByte(reader) != 10)
                    return null;

                return ReadCompound(reader);
            }
        }

        public static NBT_Compound LoadFromJSON(string json)
        {
            throw new NotImplementedException("NBT - Load From JSON");
        }

        private static NBT_Compound ReadCompound(BinaryReader reader, bool includeName = true)
        {
            NBT_Compound compound = new NBT_Compound(includeName ? ReadString(reader) : null);

            byte type;
            while ((type = ReadUByte(reader)) != 0)
                compound.Add(ReadTag(reader, type));
            return compound;
        }

        private static NBT_Tag ReadTag(BinaryReader reader, byte type, bool includeName = true)
        {
            switch (type)
            {
                case 0:
                    return new NBT_End();
                default:
                    throw new IndexOutOfRangeException();
                case 1:
                    return new NBT_Byte(includeName ? ReadString(reader) : null, ReadByte(reader));
                case 2:
                    return new NBT_Short(includeName ? ReadString(reader) : null, ReadShort(reader));
                case 3:
                    return new NBT_Int(includeName ? ReadString(reader) : null, ReadInt(reader));
                case 4:
                    return new NBT_Long(includeName ? ReadString(reader) : null, ReadLong(reader));
                case 5:
                    return new NBT_Float(includeName ? ReadString(reader) : null, ReadFloat(reader));
                case 6:
                    return new NBT_Double(includeName ? ReadString(reader) : null, ReadDouble(reader));
                case 7:
                    {
                        string name = includeName ? ReadString(reader) : null;
                        int length = ReadInt(reader);
                        sbyte[] bytes = new sbyte[length];
                        for (int i = 0; i < length; i++)
                            bytes[i] = (sbyte)reader.ReadByte();
                        return new NBT_ByteArray(name, bytes);
                    }
                case 8:
                    return new NBT_String(includeName ? ReadString(reader) : null, ReadString(reader));
                case 9:
                    return ReadList(reader, includeName);
                case 10:
                    return ReadCompound(reader, includeName);
                case 11:
                    {
                        string name = includeName ? ReadString(reader) : null;
                        int length = ReadInt(reader);
                        int[] bytes = new int[length];
                        for (int i = 0; i < length; i++)
                            bytes[i] = ReadInt(reader);
                        return new NBT_IntArray(name, bytes);
                    }
            }

        }

        private static NBT_Tag ReadList(BinaryReader reader, bool includeName = true)
        {
            string name = includeName ? ReadString(reader) : null;

            byte type = ReadUByte(reader);

            Type T = null;

            switch (type)
            {
                default:
                    throw new IndexOutOfRangeException();
                case 0:
                    T = typeof(NBT_Tag);
                    break;
                case 1:
                    T = typeof(NBT_Byte);
                    break;
                case 2:
                    T = typeof(NBT_Short);
                    break;
                case 3:
                    T = typeof(NBT_Int);
                    break;
                case 4:
                    T = typeof(NBT_Long);
                    break;
                case 5:
                    T = typeof(NBT_Float);
                    break;
                case 6:
                    T = typeof(NBT_Double);
                    break;
                case 7:
                    T = typeof(NBT_ByteArray);
                    break;
                case 8:
                    T = typeof(NBT_String);
                    break;
                case 9:
                    throw new NotSupportedException();
                case 10:
                    T = typeof(NBT_Compound);
                    break;
                case 11:
                    T = typeof(NBT_IntArray);
                    break;
            }

            /*
			Type TArray = T.MakeArrayType(1);
			dynamic arr = Activator.CreateInstance(TArray, new object[]{length});

			for(int i = 0; i < length; i++)
				arr[i] = ReadTag(reader, type, false); //Cannot cast NBT_Long to NBT_Tag (NBT_* is child of NBT_Tag)

			Type TList = typeof(NBT_List<>).MakeGenericType(T);
			return Activator.CreateInstance(TList, new object[]{name, arr}) as NBT_Tag;
			*/

            return typeof(CompoundLoader).GetMethod("ReadListGeneric", BindingFlags.NonPublic | BindingFlags.Static).MakeGenericMethod(T).Invoke(null, new object[] { reader, name, type }) as NBT_Tag;
        }

        /// <summary>
        /// This is here to solve the generic problem.
        /// </summary>
        /// <returns>Result list.</returns>
        /// <param name="reader">Source stream.</param>
        /// <param name="name">Name of the list.</param>
        /// <param name="type">Type of all elements in the list.</param>
        /// <typeparam name="T">Generic type equivalent of <paramref name="type"/>.</typeparam>
        private static NBT_List<T> ReadListGeneric<T>(BinaryReader reader, string name, byte type) where T : NBT_Tag
        {
            int length = ReadInt(reader);

            T[] arr = new T[length];

            for (int i = 0; i < length; i++)
                arr[i] = (T)ReadTag(reader, type, false);

            return new NBT_List<T>(name, arr);
        }

        private static string ReadString(BinaryReader reader)
        {
            ushort count = ReadUShort(reader);

            if (count == 0)
                return null;

            return Encoding.UTF8.GetString(reader.ReadBytes(count));
        }

        #region Read Utils

        private static sbyte ReadByte(BinaryReader reader)
        {
            return (sbyte)reader.ReadByte();
        }

        private static byte ReadUByte(BinaryReader reader)
        {
            return reader.ReadByte();
        }

        private static ushort ReadUShort(BinaryReader reader)
        {
            return BitConverter.ToUInt16(reader.ReadBytes(2).Reverse(), 0);
        }

        private static short ReadShort(BinaryReader reader)
        {
            return BitConverter.ToInt16(reader.ReadBytes(2).Reverse(), 0);
        }

        private static int ReadInt(BinaryReader reader)
        {
            return BitConverter.ToInt32(reader.ReadBytes(4).Reverse(), 0);
        }

        private static long ReadLong(BinaryReader reader)
        {
            return BitConverter.ToInt64(reader.ReadBytes(8).Reverse(), 0);
        }

        private static float ReadFloat(BinaryReader reader)
        {
            return BitConverter.ToSingle(reader.ReadBytes(4).Reverse(), 0);
        }

        private static double ReadDouble(BinaryReader reader)
        {
            return BitConverter.ToDouble(reader.ReadBytes(8).Reverse(), 0);
        }

        #endregion
    }
}