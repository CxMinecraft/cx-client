﻿using System;
using Utils.Extensions;

namespace CxClient.NBT
{
    public class NBT_Byte : NBT_Tag
    {

        #region Constructors

        public NBT_Byte(string name, sbyte value) : base(name, 1)
        {
            this.Value = value;
        }

        #endregion

        #region Variables

        public sbyte Value
        {
            get;
        }

        #endregion

        public override void Write(System.IO.BinaryWriter writer, bool onlyValue = false)
        {
            base.Write(writer, onlyValue);

            writer.Write(BitConverter.GetBytes(Value).Reverse());
        }

        public override string ToString()
        {
            return string.Format("NBT_Byte({0}): {1}", Name, Value);
        }

        public override NBT_Tag Clone()
        {
            return new NBT_Byte(Name, Value);
        }
    }
}