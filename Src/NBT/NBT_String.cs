﻿using System;
using System.Text;
using Utils.Extensions;

namespace CxClient.NBT
{
    public class NBT_String : NBT_Tag
    {

        #region Constructors

        public NBT_String(string name, string value) : base(name, 8)
        {
            if (string.IsNullOrEmpty(value))
            {
                this.Value = null;
                this.ValueBytes = new byte[0];
                this.ValueLength = 0;
            }
            else
            {
                this.Value = value;
                this.ValueBytes = Encoding.UTF8.GetBytes(value);

                if (ValueBytes.Length > ushort.MaxValue)
                    throw new ArgumentOutOfRangeException(nameof(value));

                this.ValueLength = (ushort)ValueBytes.Length;
            }
        }

        #endregion

        #region Variables

        public string Value
        {
            get;
        }

        public byte[] ValueBytes
        {
            get;
        }

        public ushort ValueLength
        {
            get;
        }

        #endregion

        public override void Write(System.IO.BinaryWriter writer, bool onlyValue = false)
        {
            base.Write(writer, onlyValue);

            writer.Write(BitConverter.GetBytes(ValueLength).Reverse());
            writer.Write(ValueBytes);
        }

        public override string ToString()
        {
            return string.Format("NBT_String({0}): {1}", Name, Value);
        }

        public override NBT_Tag Clone()
        {
            return new NBT_String(Name, Value);
        }

    }
}