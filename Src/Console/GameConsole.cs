﻿using System;
using System.Collections.Generic;
using Utils.Extensions;

namespace CxClient.GameConsole
{
    public static class Console
    {
        public const int LastLinesMaxLength = 30;

        public static string[] LastLines
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public static string Process(string line)
        {
            line = line.Trim();

            int index = line.IndexOf(' ');
            if (index == -1)
            {
                var val = GetValue(line);
                if (val == null || val.Length == 0)
                    return "<NULL>";
                return val.JoinArgs();
            }
            else
            {
                string name = line.Substring(0, index);
                string[] val = line.Substring(index + 1).Replace("\\n", "\n").Split(' ');
                SetValue(name, val);
                return null;
            }
        }

        private static Dictionary<string, string[]> _values = new Dictionary<string, string[]>();

        public static string[] GetValue(string name)
        {
            string[] arr;
            if (_values.TryGetValue(name, out arr))
                return arr;
            else
                return null;
        }

        public static void SetValue(string name, params string[] values)
        {
            var prevVal = GetValue(name);

            if (ValueChanged != null)
                ValueChanged(name, prevVal, values);

            _values[name] = values;
        }

        public delegate void ValueChange(string name, string[] prevValues, string[] newValues);

        public static event ValueChange ValueChanged;
    }
}