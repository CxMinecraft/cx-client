﻿using System;
using System.Threading;
using CxClient.Render;
using CxClient.Settings;

namespace CxClient
{
    public static class Client
    {
        internal static int MainThreadManagedId;

        public static void Main(string[] args)
        {
            MainThreadManagedId = Thread.CurrentThread.ManagedThreadId;


            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Clear();

            #region HW Info

            Console.WriteLine("================================================================");
            Console.WriteLine("|                    Machine informations                      |");
            Console.WriteLine("================================================================");

            Console.WriteLine();

            Console.Write("Command line arguments: ");
            Console.WriteLine(Environment.CommandLine);

            Console.Write("Current directory: ");
            Console.WriteLine(Environment.CurrentDirectory);

            Console.WriteLine(Environment.Is64BitOperatingSystem ? "64-bit operating system" : "32-bit operating system");

            Console.WriteLine(Environment.Is64BitProcess ? "64-bit processor" : "32-bit processor");

            Console.Write("Machine name: ");
            Console.WriteLine(Environment.MachineName);

            Console.Write("Operating system: ");
            Console.WriteLine(Environment.OSVersion.ToString());

            Console.Write("Processor count: ");
            Console.WriteLine(Environment.ProcessorCount.ToString());

            Console.Write("User name: ");
            Console.WriteLine(Environment.UserName);

            Console.Write("Seconds from system startup: ");
            Console.WriteLine((Environment.TickCount / 1000f).ToString());

            Console.WriteLine();

            Console.WriteLine("================================================================");
            Console.WriteLine("|                 End of hardware informations                 |");
            Console.WriteLine("================================================================");

            #endregion

            ClientSettings.LoadConfig("client.yml");

            #region Parse arguments

            //Log arguments
            Console.WriteLine("Command-line arguments: " + string.Join(",", args));

            //parse arguments
            foreach (string arg in args)
            {
                if (string.IsNullOrEmpty(arg))
                    continue;
                if (arg.StartsWith("--"))
                {
                    switch (arg)
                    {
                        case "--xxx=":
                            {
                                //ServerSettings.RConEnabled = arg.Substring ("--rcon=".Length) [0] == 't';
                                break;
                            }
                    }
                    //TODO long arguments
                }
                else if (arg.StartsWith("-"))
                {
                    switch (arg)
                    {
                        case "-x":
                            {
                                break;
                            }
                    }
                    //TODO short arguments
                }
                else
                {
                    //PluginManager.LoadPlugin (arg);
                }
            }

            #endregion

            #region Init Graphics

            //TODO init graphics
            RenderEngine.Init(string.Format("{0} - {1}", ClientSettings.Branch, ClientSettings.Version), -1, -1, 800, 600);//TODO load from config

            #endregion

            #region Load Mods

            //TODO load mods

            #endregion

            #region Load Scripts

            //TODO load scripts

            #endregion

            #region Init Main Menu

            //TODO start menu

            #endregion

        }

        public static void ConnectToServer(string hostname, int port = 25565, UserInfo user = null)
        {
            throw new NotImplementedException();
        }

        #region Exit / Quit

        public static void Exit(int exitCode = 0)
        {
            RenderEngine.Shutdown();

            Environment.Exit(exitCode);
        }

        public static void ExitError(string error)
        {
            Console.Error.WriteLine(error);

            Exit(-1);
        }

        #endregion
    }
}