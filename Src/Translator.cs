﻿using System;
using System.Collections.Generic;
using Utils.Configurations;
using System.IO;
using System.Text;
using CxClient.Settings;

namespace CxClient
{
    public static class Translator
    {

        public static class Languages
        {
            public const string Afrikaans = "af_ZA";
            public const string Arabic = "ar_SA";
            public const string Asturian = "ast_ES";
            public const string Azerbaijani = "az_AZ";
            public const string Bulgarian = "bg_BG";
            public const string Catalan = "ca_ES";
            public const string Czech = "cs_CZ";
            public const string Welsh = "cy_GB";
            public const string Danish = "da_DK";
            public const string German = "de_DE";
            public const string Greek = "el_GR";
            public const string AustralianEnglish = "en_AU";
            public const string CanadianEnglish = "en_CA";
            public const string BritishEnglish = "en_GB";
            public const string PirateEnglist = "en_PT";
            public const string AmericanEnglish = "en_US";
            public const string Esperanto = "eo_UY";
            public const string ArgentinianSpanish = "es_AR";
            public const string Spanish = "es_ES";
            public const string MexicanSpanish = "es_MX";
            public const string UruguayanSpanish = "es_UY";
            public const string VenezuelanSpanish = "es_VE";
            public const string Estonian = "et_EE";
            public const string Basque = "eu_ES";
            public const string Persian = "fa_IR";
            public const string Finnish = "fi_FI";
            public const string French = "fr_FR";
            public const string CanadianFrench = "fr_CA";
            public const string Irish = "ga_IE";
            public const string Galician = "gl_ES";
            public const string Manx = "gv_IM";
            public const string Hebrew = "he_IL";
            public const string Hindi = "hi_IN";
            public const string Croatian = "hr_HR";
            public const string Hungarian = "hu_HU";
            public const string Armenian = "hy_AM";
            public const string Indonesian = "id_ID";
            public const string Icelandic = "is_IS";
            public const string Italian = "it_IT";
            public const string Japanese = "ja_JP";
            public const string Georgian = "ka_GE";
            public const string Korean = "ko_KR";
            public const string Cornwall = "kw_GB";
            public const string Latin = "la_VA";
            public const string Luxembourgish = "lb_LU";
            public const string Lithuanian = "lt_LT";
            public const string Latvian = "lv_LV";
            public const string Maori = "mi_NZ";
            public const string Malay = "ms_MY";
            public const string Maltese = "mt_MT";
            public const string LowGerman = "nds_DE";
            public const string Dutch = "nl_NL";
            public const string NorwegianNyorsk = "nn_NO";
            public const string Norwegian = "no_NO";
            public const string Occitan = "oc_FR";
            public const string Polish = "pl_PL";
            public const string BrazilianPortuguese = "pt_BR";
            public const string Portuguese = "pt_PT";
            public const string Quenya = "qya_AA";
            public const string Romanian = "ro_RO";
            public const string Russian = "ru_RU";
            public const string Slovak = "sk_SK";
            public const string Slovenian = "sl_SI";
            public const string NorthernSami = "sme";
            public const string Serbian = "sr_SP";
            public const string Swedish = "sb_SE";
            public const string Thai = "th_TH";
            public const string Filipino = "tl_PH";
            public const string Klingon = "tlh_AA";
            public const string Turkish = "tr_TR";
            public const string Ukrainian = "uk_UA";
            public const string Valencian = "ca-val_ES";
            public const string Vietnamese = "vi_VN";
            public const string ChineseSimplified = "zh_CN";
            public const string ChineseTraditional = "zh_TW";
        }

        private static readonly Dictionary<string, Configuration> _groups = new Dictionary<string, Configuration>();

        private static bool Load(string group, string lang)
        {
            string gl = GetDictionaryKey(group, lang);

            string path = Path.Combine(ClientSettings.LanguageDirectory, lang, group + ".lang");

            if (!File.Exists(path))
                return false;

            if (_groups.ContainsKey(gl))
                _groups[gl] = _groups[gl] + YAML.Load(File.ReadAllText(path, Encoding.UTF8));
            else
                _groups[gl] = YAML.Load(File.ReadAllText(path, Encoding.UTF8));
            return true;
        }

        private static string GetDictionaryKey(string group, string lang)
        {
            return string.Format("{0}.{1}", lang.ToLower(), group.ToLower());
        }

        public static string Translate(string group, string key, string lang = Languages.AmericanEnglish)
        {
            string gl = GetDictionaryKey(group, lang);

            if (!_groups.ContainsKey(gl) && !Load(group, lang))
            {
                if (lang == Languages.AmericanEnglish)
                    return key;
                else
                    Translate(group, key, Languages.AmericanEnglish);
            }
            Configuration g = _groups[gl];
            if (g == null)
                return key;
            return g.GetString(key, key);
        }

        public static void ChangeLanguage(string lang)
        {
            var keys = new List<string>(_groups.Keys);

            _groups.Clear();

            foreach (var k in keys)
                Load(k, lang);

            ClientSettings._language = lang;
        }
    }
}