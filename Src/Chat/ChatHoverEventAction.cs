﻿using System;

namespace CxClient.Chat
{
    public enum ChatHoverEventAction
    {
        NONE = 0,
        ShowText,
        ShowAchievement,
        ShowItem
    }
}