﻿using System;
using System.Linq;

namespace CxClient.Chat
{
    public class ChatMessagePlain : ChatMessage
    {

        #region Constructors

        public ChatMessagePlain(string plainText)
        {
            this.PlainText = plainText;
        }

        #endregion

        #region Variables

        public string PlainText
        {
            get;
        }

        #endregion

        #region implemented abstract members of ChatMessage

        protected override string ChildJSON
        {
            get
            {
                return string.Format("\"text\": \"{0}\",", PlainText);
            }
        }

        #endregion

        public bool IsOnlyText
        {
            get
            {
                return !base.Extra.Any() && !Bold && !Italic && !Underlined && !Strikethrough && !Obfuscated && Color == ChatColor.NONE && ClickAction == ChatClickEventAction.NONE && HoverAction == ChatHoverEventAction.NONE && string.IsNullOrWhiteSpace(Insertion);
            }
        }

        public override string ToString()
        {
            return IsOnlyText ? string.Format("\"{0}\"", PlainText) : ToJSON();
        }
    }
}