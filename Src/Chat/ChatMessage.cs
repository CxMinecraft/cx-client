﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Utils.Configurations;

namespace CxClient.Chat
{
    public abstract class ChatMessage
    {
        #region Constructors

        protected ChatMessage(ChatColor color = ChatColor.NONE, bool bold = false, bool italic = false, bool underlined = false, bool strikethrough = false, bool obfuscated = false)
        {
            this.Color = color;
        }

        #endregion

        public override string ToString()
        {
            return ToJSON();
        }

        #region Variables

        public ChatColor Color
        {
            get;
            set;
        } = ChatColor.NONE;

        public bool Bold
        {
            get;
            set;
        } = false;

        public bool Italic
        {
            get;
            set;
        } = false;

        public bool Underlined
        {
            get;
            set;
        } = false;

        public bool Strikethrough
        {
            get;
            set;
        } = false;

        public bool Obfuscated
        {
            get;
            set;
        } = false;

        public string Insertion
        {
            get;
            set;
        } = null;

        #endregion

        #region Events

        public ChatClickEventAction ClickAction
        {
            get;
            private set;
        } = ChatClickEventAction.NONE;

        public string ClickValue
        {
            get;
            private set;
        }

        public void AddClickEvent(ChatClickEventAction action, string value)
        {
            throw new NotImplementedException("Chat Message - Add Click Event");
        }

        public ChatHoverEventAction HoverAction
        {
            get;
            private set;
        } = ChatHoverEventAction.NONE;

        public string HoverValue
        {
            get;
            private set;
        }

        public void AddHoverEvent(ChatHoverEventAction action, string value)
        {
            throw new NotImplementedException("Chat Message - Add Hover Event");
        }

        public void AddHoverEvent(ChatHoverEventAction action, ItemStack value)
        {
            throw new NotImplementedException("Chat Message - Add Hover Event");
        }

        public void AddHoverEvent(ChatHoverEventAction action, Achievement value)
        {
            throw new NotImplementedException("Chat Message - Add Hover Event");
        }

        #endregion

        #region Extra

        public List<ChatMessage> Extra = new List<ChatMessage>();

        public void AddExtra(params ChatMessage[] messages)
        {
            Extra.AddRange(messages);
        }

        #endregion

        #region JSON

        protected abstract string ChildJSON
        {
            get;
        }

        public string ToJSON()
        {
            var json = new StringBuilder();
            json.Append('{');

            json.Append(ChildJSON);

            //extra
            if (Extra.Any())
            {
                json.Append("\"extra\":[");

                json.Append(Extra[0].ToString());
                for (int i = 1; i < Extra.Count; i++)
                {
                    json.Append(',');
                    json.Append(Extra[i].ToString());
                }

                json.Append("],");
            }

            //bold
            json.Append("\"bold\":");
            json.Append(Bold ? "true" : "false");
            json.Append(',');

            //bold
            json.Append("\"italic\":");
            json.Append(Italic ? "true" : "false");
            json.Append(',');

            //bold
            json.Append("\"underlined\":");
            json.Append(Underlined ? "true" : "false");
            json.Append(',');

            //bold
            json.Append("\"strikethrough\":");
            json.Append(Strikethrough ? "true" : "false");
            json.Append(',');

            //bold
            json.Append("\"obfuscated\":");
            json.Append(Obfuscated ? "true" : "false");
            json.Append(',');

            //color
            if (Color != ChatColor.NONE)
            {
                json.Append("\"color\":\"");
                json.Append(Color.GetTechnicalName());
                json.Append("\",\"");
            }

            //TODO click event

            //TODO hoverEvent

            if (!string.IsNullOrWhiteSpace(Insertion))
            {
                json.Append("\"insertion\":\"");
                json.Append(Insertion);
                json.Append("\",\"");
            }

            json.Append('}');
            return json.ToString();
        }

        public static ChatMessage FromJSON(string json)
        {
            Configuration cfg = JSON.Load(json);
            ChatMessage chat;

            if (cfg.Contains("text"))
                chat = new ChatMessagePlain(cfg.GetString("text"));
            else if (cfg.Contains("translate"))
                chat = new ChatMessageTranslate(cfg.GetString("translate"), cfg.GetStringArray("with"));
            else
                return null;

            if (cfg.Contains("bold"))
                chat.Bold = cfg.GetBool("bold");
            if (cfg.Contains("italic"))
                chat.Italic = cfg.GetBool("italic");
            if (cfg.Contains("underlined"))
                chat.Underlined = cfg.GetBool("underlined");
            if (cfg.Contains("strikethrough"))
                chat.Strikethrough = cfg.GetBool("strikethrough");
            if (cfg.Contains("obfuscated"))
                chat.Obfuscated = cfg.GetBool("obfuscated");

            if (cfg.Contains("insertion"))
                chat.Insertion = cfg.GetString("insertion");

            if (cfg.Contains("color"))
                chat.Color = ChatColorUtils.FromTechnicalName(cfg.GetString("color"));

            //TODO click event

            //TODO hoverEvent

            //TODO extra

            return chat;
        }

        public static bool IsValidJSON(string json) => JSON.IsValid(json);

        #endregion

        #region Old & New format

        public const char OldFormatChar = '§';

        public bool IsSameFormat(ChatMessage chat)
        {
            if (chat == null)
                return false;
            return this.Color == chat.Color && this.Bold == chat.Bold && this.Italic == chat.Italic && this.Underlined == chat.Underlined && this.Strikethrough == chat.Strikethrough && this.Obfuscated == chat.Obfuscated;
        }

        public string OldFormatFormatting
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                if (Color != ChatColor.NONE)
                {
                    sb.Append(OldFormatChar);
                    sb.Append(Color.GetColorChar());
                }
                else
                {
                    sb.Append(OldFormatChar);
                    sb.Append('r');
                }

                if (Obfuscated)
                {
                    sb.Append(OldFormatChar);
                    sb.Append('k');
                }

                if (Bold)
                {
                    sb.Append(OldFormatChar);
                    sb.Append('l');
                }

                if (Strikethrough)
                {
                    sb.Append(OldFormatChar);
                    sb.Append('m');
                }

                if (Underlined)
                {
                    sb.Append(OldFormatChar);
                    sb.Append('n');
                }

                if (Italic)
                {
                    sb.Append(OldFormatChar);
                    sb.Append('o');
                }

                return sb.ToString();
            }
        }

        public string OldFormat
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                if (this is ChatMessagePlain)
                {
                    sb.Append(OldFormatFormatting);
                    sb.Append(((ChatMessagePlain)this).PlainText);
                }

                ChatMessage chat = this;

                for (int i = 0; i < Extra.Count; i++)
                {
                    ChatMessage ch = Extra[i];

                    if (ch is ChatMessagePlain)
                    {
                        if (!chat.IsSameFormat(ch))
                            sb.Append(OldFormatFormatting);

                        sb.Append(((ChatMessagePlain)ch).PlainText);
                    }
                    chat = Extra[i];
                }

                return sb.ToString();
            }
        }

        public static ChatMessage FromOldFormat(string msg, char formatChar = OldFormatChar)
        {
            int lastFormatIndex = -1;
            int lastMsgIndex = -1;

            List<Tuple<string, string>> messages = new List<Tuple<string, string>>();

            for (int i = 0; i < msg.Length; i++)
            {
                if (lastMsgIndex == -1)
                {
                    if (msg[i] == formatChar)
                    {
                        if (lastFormatIndex == -1)
                            lastFormatIndex = i;
                        i++;
                        continue;
                    }
                    else
                    {
                        lastMsgIndex = i;
                        continue;
                    }
                }
                else
                {
                    if (msg[i] == formatChar)
                    {
                        string formatting = msg.Substring(lastFormatIndex, lastMsgIndex - lastFormatIndex).Replace(formatChar.ToString(), "");
                        messages.Add(new Tuple<string, string>(formatting, msg.Substring(lastMsgIndex, i - lastMsgIndex)));

                        lastFormatIndex = -1;
                        lastMsgIndex = -1;
                    }
                    else
                        continue;
                }
            }

            if (!messages.Any())
                return null;

            ChatMessagePlain chat = new ChatMessagePlain(messages[0].Item2);
            chat.ApplyOldFormatChars(messages[0].Item1);

            StringBuilder formats = new StringBuilder(messages[0].Item1);

            for (int i = 1; i < messages.Count; i++)
            {
                ChatMessagePlain ch = new ChatMessagePlain(messages[i].Item2);

                formats.Append(messages[i].Item1);
                ch.ApplyOldFormatChars(formats.ToString());

                chat.AddExtra(ch);
            }

            return chat;
        }

        public void ApplyOldFormatChars(string formatting)
        {
            formatting = formatting.Replace(OldFormatChar.ToString(), "");
            {
                int index_r = formatting.LastIndexOf('r');
                if (index_r != -1)
                    formatting.Substring(index_r);
                int index_color = formatting.LastIndexOfAny(ChatColorUtils.Chars);
                if (index_color != 0)
                    formatting.Substring(index_color);
            }

            for (int i = 0; i < formatting.Length; i++)
            {
                char c = formatting[i];
                switch (c)
                {
                    case 'k':
                        Obfuscated = true;
                        break;
                    case 'l':
                        Bold = true;
                        break;
                    case 'm':
                        Strikethrough = true;
                        break;
                    case 'n':
                        Underlined = true;
                        break;
                    case 'o':
                        Italic = true;
                        break;
                    case 'r':
                        Obfuscated = false;
                        Bold = false;
                        Strikethrough = false;
                        Underlined = false;
                        Italic = false;
                        break;
                    default:
                        Color = ChatColorUtils.FromColorChar(c);
                        break;
                }
            }
        }

        #endregion
    }
}