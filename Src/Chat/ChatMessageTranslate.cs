﻿using System;
using System.Text;

namespace CxClient.Chat
{
    public class ChatMessageTranslate : ChatMessage
    {

        #region Constructors

        public ChatMessageTranslate(string translate, params string[] with)
        {
            this.Translate = translate;
            this.With = with;
        }

        #endregion

        #region Variables

        public string Translate
        {
            get;
        }

        public string[] With
        {
            get;
        }

        #endregion

        #region implemented abstract members of ChatMessage

        protected override string ChildJSON
        {
            get
            {
                var translate = new StringBuilder(string.Format("\"translate\": \"{0}\",", Translate));
                if (With != null && With.Length != 0)
                {
                    translate.Append("\"with\":[\"");
                    //translate.Append(string.Join("\",\""));
                    translate.Append(With[0]);
                    for (int i = 1; i < With.Length; i++)
                    {
                        translate.Append("\",\"");
                        translate.Append(With[i]);
                    }
                    translate.Append("\"],");
                }
                return translate.ToString();
            }
        }

        #endregion

    }
}