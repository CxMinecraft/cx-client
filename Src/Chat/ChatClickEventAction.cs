﻿using System;

namespace CxClient.Chat
{
    public enum ChatClickEventAction
    {
        NONE = 0,
        OpenUrl,
        OpenFile,
        RunCommand,
        SuggestCommand
    }
}