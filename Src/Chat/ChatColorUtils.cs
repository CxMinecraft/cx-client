﻿using System;
using System.Drawing;
using Utils.Extensions;

namespace CxClient.Chat
{
    public static class ChatColorUtils
    {

        #region Get Values

        public static char GetColorChar(this ChatColor color)
        {
            switch (color)
            {
                default:
                case ChatColor.NONE:
                    return ' ';
                case ChatColor.Black:
                    return '0';
                case ChatColor.DarkBlue:
                    return '1';
                case ChatColor.DarkGreen:
                    return '2';
                case ChatColor.DarkAqua:
                    return '3';
                case ChatColor.DarkRed:
                    return '4';
                case ChatColor.DarkPurple:
                    return '5';
                case ChatColor.Gold:
                    return '6';
                case ChatColor.Gray:
                    return '7';
                case ChatColor.DarkGray:
                    return '8';
                case ChatColor.Blue:
                    return '9';
                case ChatColor.Green:
                    return 'a';
                case ChatColor.Aqua:
                    return 'b';
                case ChatColor.Red:
                    return 'c';
                case ChatColor.LightPurple:
                    return 'd';
                case ChatColor.Yellow:
                    return 'e';
                case ChatColor.White:
                    return 'f';
            }
        }

        public static string GetTechnicalName(this ChatColor color)
        {
            switch (color)
            {
                default:
                case ChatColor.NONE:
                    return "";
                case ChatColor.Black:
                    return "black";
                case ChatColor.DarkBlue:
                    return "dark_blue";
                case ChatColor.DarkGreen:
                    return "dark_green";
                case ChatColor.DarkAqua:
                    return "dark_aqua";
                case ChatColor.DarkRed:
                    return "dark_red";
                case ChatColor.DarkPurple:
                    return "dark_purple";
                case ChatColor.Gold:
                    return "gold";
                case ChatColor.Gray:
                    return "gray";
                case ChatColor.DarkGray:
                    return "dark_gray";
                case ChatColor.Blue:
                    return "blue";
                case ChatColor.Green:
                    return "green";
                case ChatColor.Aqua:
                    return "aqua";
                case ChatColor.Red:
                    return "red";
                case ChatColor.LightPurple:
                    return "light_purple";
                case ChatColor.Yellow:
                    return "yellow";
                case ChatColor.White:
                    return "white";
            }
        }

        public static Color GetForegroundColor(this ChatColor color)
        {
            switch (color)
            {
                case ChatColor.Black:
                    return Color.FromArgb(0, 0, 0);
                case ChatColor.DarkBlue:
                    return Color.FromArgb(0, 0, 170);
                case ChatColor.DarkGreen:
                    return Color.FromArgb(0, 170, 0);
                case ChatColor.DarkAqua:
                    return Color.FromArgb(0, 170, 170);
                case ChatColor.DarkRed:
                    return Color.FromArgb(170, 0, 0);
                case ChatColor.DarkPurple:
                    return Color.FromArgb(170, 0, 170);
                case ChatColor.Gold:
                    return Color.FromArgb(255, 170, 0);
                case ChatColor.Gray:
                    return Color.FromArgb(170, 170, 170);
                case ChatColor.DarkGray:
                    return Color.FromArgb(85, 85, 85);
                case ChatColor.Blue:
                    return Color.FromArgb(85, 85, 255);
                case ChatColor.Green:
                    return Color.FromArgb(85, 255, 85);
                case ChatColor.Aqua:
                    return Color.FromArgb(85, 255, 255);
                case ChatColor.Red:
                    return Color.FromArgb(255, 85, 85);
                case ChatColor.LightPurple:
                    return Color.FromArgb(255, 85, 255);
                case ChatColor.Yellow:
                    return Color.FromArgb(255, 255, 85);
                case ChatColor.White:
                    return Color.FromArgb(255, 255, 255);
                default:
                    return default(Color);
            }
        }

        public static int GetForegroundHex(this ChatColor color)
        {
            switch (color)
            {
                case ChatColor.Black:
                    return 0x000000;
                case ChatColor.DarkBlue:
                    return 0x0000AA;
                case ChatColor.DarkGreen:
                    return 0x00AA00;
                case ChatColor.DarkAqua:
                    return 0x00AAAA;
                case ChatColor.DarkRed:
                    return 0xAA0000;
                case ChatColor.DarkPurple:
                    return 0xAA00AA;
                case ChatColor.Gold:
                    return 0xFFAA00;
                case ChatColor.Gray:
                    return 0xAAAAAA;
                case ChatColor.DarkGray:
                    return 0x555555;
                case ChatColor.Blue:
                    return 0x5555FF;
                case ChatColor.Green:
                    return 0x55FF55;
                case ChatColor.Aqua:
                    return 0x55FFFF;
                case ChatColor.Red:
                    return 0xFF5555;
                case ChatColor.LightPurple:
                    return 0xFF55FF;
                case ChatColor.Yellow:
                    return 0xFFFF55;
                case ChatColor.White:
                    return 0xFFFFFF;
                default:
                    return default(int);
            }
        }

        public static Color GetBackgroundColor(this ChatColor color)
        {
            switch (color)
            {
                case ChatColor.Black:
                    return Color.FromArgb(0, 0, 0);
                case ChatColor.DarkBlue:
                    return Color.FromArgb(0, 0, 42);
                case ChatColor.DarkGreen:
                    return Color.FromArgb(0, 42, 0);
                case ChatColor.DarkAqua:
                    return Color.FromArgb(0, 42, 42);
                case ChatColor.DarkRed:
                    return Color.FromArgb(42, 0, 0);
                case ChatColor.DarkPurple:
                    return Color.FromArgb(42, 0, 42);
                case ChatColor.Gold:
                    return Color.FromArgb(42, 42, 0);
                case ChatColor.Gray:
                    return Color.FromArgb(42, 42, 42);
                case ChatColor.DarkGray:
                    return Color.FromArgb(21, 21, 21);
                case ChatColor.Blue:
                    return Color.FromArgb(21, 21, 63);
                case ChatColor.Green:
                    return Color.FromArgb(21, 63, 21);
                case ChatColor.Aqua:
                    return Color.FromArgb(21, 63, 63);
                case ChatColor.Red:
                    return Color.FromArgb(63, 21, 21);
                case ChatColor.LightPurple:
                    return Color.FromArgb(63, 21, 63);
                case ChatColor.Yellow:
                    return Color.FromArgb(63, 63, 21);
                case ChatColor.White:
                    return Color.FromArgb(63, 63, 63);
                default:
                    return default(Color);
            }
        }

        public static int GetBackgroundHex(this ChatColor color)
        {
            switch (color)
            {
                case ChatColor.Black:
                    return 0x000000;
                case ChatColor.DarkBlue:
                    return 0x00002A;
                case ChatColor.DarkGreen:
                    return 0x002A00;
                case ChatColor.DarkAqua:
                    return 0x002A2A;
                case ChatColor.DarkRed:
                    return 0x2A0000;
                case ChatColor.DarkPurple:
                    return 0x2A002A;
                case ChatColor.Gold:
                    return 0x2A2A00;
                case ChatColor.Gray:
                    return 0x2A2A2A;
                case ChatColor.DarkGray:
                    return 0x151515;
                case ChatColor.Blue:
                    return 0x15153F;
                case ChatColor.Green:
                    return 0x153F15;
                case ChatColor.Aqua:
                    return 0x153F3F;
                case ChatColor.Red:
                    return 0x3F1515;
                case ChatColor.LightPurple:
                    return 0x3F153F;
                case ChatColor.Yellow:
                    return 0x3F3F15;
                case ChatColor.White:
                    return 0x3F3F3F;
                default:
                    return default(int);
            }
        }

        #endregion

        #region Chars

        public static bool IsColorChar(this char c)
        {
            return Chars.Contains(c);
        }

        public static char[] Chars
        {
            get;
        }

        static ChatColorUtils()
        {
            Chars = "0123456789abcdef".ToCharArray(); ;
        }

        #endregion

        #region From values

        public static ChatColor FromTechnicalName(string name)
        {
            switch (name)
            {
                case "black":
                    return ChatColor.Black;
                case "dark_blue":
                    return ChatColor.DarkBlue;
                case "dark_green":
                    return ChatColor.DarkGreen;
                case "dark_aqua":
                    return ChatColor.DarkAqua;
                case "dark_red":
                    return ChatColor.DarkRed;
                case "dark_purple":
                    return ChatColor.DarkPurple;
                case "gold":
                    return ChatColor.Gold;
                case "gray":
                case "grey":
                    return ChatColor.Gray;
                case "dark_gray":
                    return ChatColor.DarkGray;
                case "blue":
                    return ChatColor.Blue;
                case "green":
                    return ChatColor.Green;
                case "aqua":
                    return ChatColor.Aqua;
                case "red":
                    return ChatColor.Red;
                case "light_purple":
                    return ChatColor.LightPurple;
                case "yellow":
                    return ChatColor.Yellow;
                case "white":
                    return ChatColor.White;
                default:
                    return ChatColor.NONE;
            }
        }

        public static ChatColor FromColorChar(char c)
        {
            switch (c)
            {
                case '0':
                    return ChatColor.Black;
                case '1':
                    return ChatColor.DarkBlue;
                case '2':
                    return ChatColor.DarkGreen;
                case '3':
                    return ChatColor.DarkAqua;
                case '4':
                    return ChatColor.DarkRed;
                case '5':
                    return ChatColor.DarkPurple;
                case '6':
                    return ChatColor.Gold;
                case '7':
                    return ChatColor.Gray;
                case '8':
                    return ChatColor.DarkGray;
                case '9':
                    return ChatColor.Blue;
                case 'a':
                case 'A':
                    return ChatColor.Green;
                case 'b':
                case 'B':
                    return ChatColor.Aqua;
                case 'c':
                case 'C':
                    return ChatColor.Red;
                case 'd':
                case 'D':
                    return ChatColor.LightPurple;
                case 'e':
                case 'E':
                    return ChatColor.Yellow;
                case 'f':
                case 'F':
                    return ChatColor.White;
                default:
                    return ChatColor.NONE;
            }
        }

        #endregion

    }
}