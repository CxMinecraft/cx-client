﻿namespace CxClient.Enums
{
    public enum ChatMode
    {
        /// <summary>
        /// Full chat.<br>
        /// Client can send (and receive if server supports it) both messages and commands.
        /// </summary>
        Enabled = 0,
        /// <summary>
        /// Chat only for commands.<br>
        /// Client cannot send messages (and will not receive them if server are configured well).<br>
        /// Commands are still supported.
        /// </summary>
        CommandsOnly = 1,
        /// <summary>
        /// Works as normal chat but is visible only when opened.
        /// </summary>
        Hidden = 2
    }
}