﻿using System;
namespace CxClient
{
    public enum ShadowMode
    {
        /// <summary>
        /// No shadows (full bright).<br>
        /// May be considered as cheating.
        /// </summary>
        None,
        /// <summary>
        /// Block takes it's lightness.
        /// </summary>
        Self,
        /// <summary>
        /// Block takes lightness by faces.
        /// </summary>
        Simple,
        /// <summary>
        /// Lightness is calculated in runtime.<br>
        /// This may require a lot of graphic power.
        /// </summary>
        Realtime
    }
}