﻿namespace CxClient.Enums
{
    public enum MenuType
    {
        /// <summary>
        /// Unknown, fall back to <see cref="CxClient.Enums.MenuType.Vanilla"/>
        /// </summary>
        Other = 0,

        /// <summary>
        /// Normal vanilla menu with blured background
        /// </summary>
        Vanilla = 11,
        /// <summary>
        /// Normal vanilla menu with static background
        /// </summary>
        Vanilla2D = 12,
        /// <summary>
        /// Normal vanilla with 3D background (rendered part of world, probably with moving entities)
        /// </summary>
        Vanilla3D = 13,

        /// <summary>
        /// Source Engine (FPS) menu with blured background
        /// </summary>
        Source = 21,
        /// <summary>
        /// Source Engine (FPS) menu with static background
        /// </summary>
        Source2D = 22,
        /// <summary>
        /// Source Engine (FPS) menu with 3D background (rendered part of world, probably with moving entities)
        /// </summary>
        Source3D = 23,

        /// <summary>
        /// Small map with menu signs
        /// </summary>
        World3D = 33
    }
}