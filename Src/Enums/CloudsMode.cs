﻿using System;
namespace CxClient.Enums
{
    public enum CloudsMode
    {
        /// <summary>
        /// No clouds<br>
        /// <see cref="CxClient.Settings.ClientSettings.CloudsEnabled"/> when set to <see cref="false"/>
        /// </summary>
        None = 0,
        /// <summary>
        /// 2D plane clouds.
        /// </summary>
        Simple2D = 12,
        /// <summary>
        /// 3D cube clouds.
        /// </summary>
        Simple3D = 13,
        Complex2D = 22,
        Complex3D = 23
    }
}