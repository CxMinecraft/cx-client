﻿namespace CxClient
{
    [System.Flags]
    public enum SkinParts
    {
        NONE = 0x00,
        FULL = 0xFF,

        Cape = 0x01,
        Jacket = 0x02,
        LeftSleeve = 0x04,
        RightSleeve = 0x08,
        LeftPants = 0x10,
        RightPants = 0x20,
        Hat = 0x40,
        Unused = 0x80
    }
}
