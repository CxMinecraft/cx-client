﻿using System;
namespace CxClient
{
    public enum WindowMode
    {
        WindowWithBorder = 0,
        WindowWithoutBorder = 1,
        Fullscreen = 10,
        DesktopFriendlyFullscreen = 11
    }
}