﻿using System;
namespace CxClient.Screen
{
    public interface IScreen
    {
        void Init();
        void Shutdown();

        void Update(double delta);
        void Render();
    }
}