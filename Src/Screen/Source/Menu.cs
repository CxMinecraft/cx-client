﻿using System;
using Utils.Extensions;

namespace CxClient.Screen.Source
{
    public class Menu : IScreen
    {
        public IMenuBackground Background
        {
            get;
            set;
        }

        public Menu(IMenuBackground background, params string[] servers)
        {
            this.Background = background;

            Items = servers.Reverse().BuildWith(new string[]
            {
                "",
                "Multiplayer",
                "Accounts",
                "Options",
                "Quit",
            });
        }

        #region IScreen

        public void Init()
        {
            throw new NotImplementedException();
        }

        public void Shutdown()
        {
            throw new NotImplementedException();
        }

        public void Update(double delta)
        {
            throw new NotImplementedException();
        }

        public void Render()
        {
            throw new NotImplementedException();
        }

        #endregion

        private string[] Items;

        private void ButtonClicked(int index)
        {
            if (index == Items.Length - 1)//Quit
            {

            }
            else if (index == Items.Length - 2)//Options
            {

            }
            else if (index == Items.Length - 3)//Accounts
            {

            }
            else if (index == Items.Length - 4)//Multiplayer
            {

            }
            else if (index == Items.Length - 5)//Spacer
            {
            }
            else
            {
                if (ServerClicked != null)
                    ServerClicked(index, Items[index]);
            }
        }

        public delegate void ServerClickedDelegate(int index, string name);
        public event ServerClickedDelegate ServerClicked;
    }
}