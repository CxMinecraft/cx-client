﻿using System;
using System.Runtime.InteropServices;

namespace CxClient
{
    public class Shader
    {
        private Shader(int id)
        {
            this.Id = id;
        }

        public int Id
        {
            get;
        }

        public void Activate() => Shader.Activate(Id);

        public void Trash() => Shader.Trash(Id);

        [DllImport("CxClient_render.dll", EntryPoint = "Shader_Create", CharSet = CharSet.Unicode)]
        public static extern int CreateShader([MarshalAs(UnmanagedType.LPStr)]string content);

        [DllImport("CxClient_render.dll", EntryPoint = "Shader_Activate", CharSet = CharSet.Unicode)]
        private static extern int Activate(int id);

        [DllImport("CxClient_render.dll", EntryPoint = "Shader_Trash", CharSet = CharSet.Unicode)]
        private static extern int Trash(int id);
    }
}