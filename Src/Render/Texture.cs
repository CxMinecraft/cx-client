﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace CxClient
{
    public class Texture
    {
        private Texture(int id)
        {
            this.Id = id;
        }

        public int Id
        {
            get;
        }

        public void Trash() => Texture.Trash(Id);



        [DllImport("CxClient_render.dll", EntryPoint = "Texture_Create_24RGB", CharSet = CharSet.Unicode)]
        private static unsafe extern int CreateTextureRGB(uint width, uint height, byte* data);

        [DllImport("CxClient_render.dll", EntryPoint = "Texture_Create_32ARGB", CharSet = CharSet.Unicode)]
        private static unsafe extern int CreateTextureARGB(uint width, uint height, byte* data);

        public static Texture CreateTextureRGB(Bitmap image)
        {
            var data = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            int id;
            unsafe
            {
                id = CreateTextureRGB((uint)image.Width, (uint)image.Height, (byte*)data.Scan0.ToPointer());
            }
            return new Texture(id);
        }

        public static Texture CreateTextureARGB(Bitmap image)
        {
            var data = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            int id;
            unsafe
            {
                id = CreateTextureRGB((uint)image.Width, (uint)image.Height, (byte*)data.Scan0.ToPointer());
            }
            return new Texture(id);
        }

        [DllImport("CxClient_render.dll", EntryPoint = "Texture_Trash", CharSet = CharSet.Unicode)]
        private static extern int Trash(int id);
    }
}