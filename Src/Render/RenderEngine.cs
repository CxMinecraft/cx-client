﻿using System;
using System.Runtime.InteropServices;
using Utils.Configurations;

namespace CxClient.Render
{
    public static class RenderEngine
    {
        [DllImport("CxClient_render.dll", EntryPoint = "Init", CharSet = CharSet.Unicode)]
        public static extern void Init(string title, int x, int y, uint width, uint height);
        [DllImport("CxClient_render.dll", EntryPoint = "Shutdown", CharSet = CharSet.Unicode)]
        public static extern void Shutdown();
        [DllImport("CxClient_render.dll", EntryPoint = "IsClosing", CharSet = CharSet.Unicode)]
        public static extern bool IsClosing();

        #region Loaders

        public static Texture LoadTexture(string path)
        {
            return null;
        }

        public static IModel LoadModel(string path)
        {
            return null;
        }

        public static Shader LoadShader(string path)
        {
            return null;
        }

        #endregion

        #region 2D render

        [DllImport("CxClient_render.dll", EntryPoint = "RenderLine2D", CharSet = CharSet.Unicode)]
        public static extern void RenderLine2D(int x0, int y0, int x1, int y1, float width);

        public static void RenderLine2D(int x0, int y0, int x1, int y1) => RenderLine2D(x0, y0, x1, y1, 1);

        [DllImport("CxClient_render.dll", EntryPoint = "RenderTexture2D", CharSet = CharSet.Unicode)]
        private static extern void RenderTexture2D(int x, int y, int width, int height, int texture);

        public static void RenderTexture2D(int x, int y, int width, int height, Texture texture) => RenderTexture2D(x, y, width, height, texture.Id);

        [DllImport("CxClient_render.dll", EntryPoint = "Tile", CharSet = CharSet.Unicode)]
        public static extern void Tile(int x, int y, int width, int height, Texture texture, int tileX, int tileY, int totalX, int totalY);

        [DllImport("CxClient_render.dll", EntryPoint = "TiledBox", CharSet = CharSet.Unicode)]
        public static extern void TiledBox(int x, int y, int width, int height, Texture texture);

        [DllImport("CxClient_render.dll", EntryPoint = "RenderText2D", CharSet = CharSet.Unicode)]
        public static extern void RenderText2D(int x, int y, string text, Align.Vertical align = Align.Vertical.Center);

        [DllImport("CxClient_render.dll", EntryPoint = "TextWidth", CharSet = CharSet.Unicode)]
        public static extern int TextWidth(string text);

        #endregion

    }
}