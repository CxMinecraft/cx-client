﻿using System;
using CxClient.Render;

namespace CxClient
{
    public static class GUI
    {
        public static void Texture(int x, int y, int width, int height, Texture texture)
        {
            RenderEngine.RenderTexture2D(x, y, width, height, texture);
        }

        public static void Tile(int x, int y, int width, int height, Texture texture, int tileX, int tileY, int totalX, int totalY)
        {
            RenderEngine.Tile(x, y, width, height, texture, tileX, tileY, totalX, totalY);
        }

        public static void TiledBox(int x, int y, int width, int height, Texture texture)
        {
            RenderEngine.TiledBox(x, y, width, height, texture);
        }

        public static void Text(int x, int y, string text, Align.Vertical align = Align.Vertical.Center)
        {
            RenderEngine.RenderText2D(x, y, text, align);
        }

        #region Vanilla

        public static Texture VanillaButtonClicked;
        public static Texture VanillaButtonHover;
        public static Texture VanillaButtonNone;

        public static Texture VanillaTextareaActive;
        public static Texture VanillaTextareaNone;

        public const int VanillaBorderWidth = 4;
        public const int VanillaBorderWidth2 = VanillaBorderWidth * 2;
        public const int VanillaMinHeight = 18;
        public const int VanillaMinHeightWithBorder = VanillaMinHeight + VanillaBorderWidth2;

        public static void ButtonVanilla(int x, int y, string text, bool hover = false, bool clicked = false, int width = -1, int height = 18 + 8)
        {
            if (height <= 0)
                height = VanillaMinHeightWithBorder;
            if (width <= 0)
                width = RenderEngine.TextWidth(text) + VanillaBorderWidth2;

            Texture texture = clicked ? VanillaButtonClicked : (hover ? VanillaButtonHover : VanillaButtonNone);

            TiledBox(x, y, width, height, texture);

            if (!string.IsNullOrWhiteSpace(text))
                Text(x + (width / 2), y + (height / 2), text, Align.Vertical.Center);
        }

        public static void TextareaVanilla(int x, int y, string text, int cursorPos = -1, bool active = false, int width = -1, int height = 18 + 8)
        {
            int textWidth = RenderEngine.TextWidth(text);

            if (height <= 0)
                height = VanillaMinHeightWithBorder;
            if (width <= 0)
                width = textWidth + VanillaBorderWidth2;
            if (cursorPos < 0)
                cursorPos = text.Length + (cursorPos % text.Length);

            Texture texture = active ? VanillaTextareaActive : VanillaTextareaNone;

            TiledBox(x, y, width, height, texture);


            if (textWidth > width - VanillaBorderWidth2)
            {
                throw new NotImplementedException();//TODO move text
            }
            else
            {
                Text(x + (width / 2), y + (height / 2), text, Align.Vertical.Center);

                int cursorX = cursorPos == 0 ? 0 : RenderEngine.TextWidth(text.Substring(0, cursorPos));
                RenderEngine.RenderLine2D(x + cursorX, y, x + cursorX, y + width);
            }
        }

        #endregion

        #region Source

        public static Texture SourceButtonClicked;
        public static Texture SourceButtonHover;
        public static Texture SourceButtonNone;

        public const int SourceBorderWidth = 4;
        public const int SourceBorderWidth2 = SourceBorderWidth * 2;
        public const int SourceMinHeight = 18;
        public const int SourceMinHeightWithBorder = SourceMinHeight + SourceBorderWidth2;

        public static void ButtonSource(int x, int y, string text, bool hover = false, bool clicked = false, int width = -1, int height = 18 + 8)
        {
            if (height <= 0)
                height = SourceMinHeightWithBorder;
            if (width <= 0)
                width = RenderEngine.TextWidth(text) + SourceBorderWidth2;

            Texture texture = clicked ? SourceButtonClicked : (hover ? SourceButtonHover : SourceButtonNone);

            TiledBox(x, y, width, height, texture);

            if (!string.IsNullOrWhiteSpace(text))
                Text(x + (width / 2), y + (height / 2), text, Align.Vertical.Center);
        }

        #endregion

    }
}