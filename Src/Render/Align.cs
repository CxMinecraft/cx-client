﻿using System;
namespace CxClient
{
    public static class Align
    {
        public enum Vertical
        {
            Left = -1,
            Center = 0,
            Right = 1
        }

        public enum Horizontal
        {
            Top = -1,
            Middle = 0,
            Bottom = 1
        }
    }
}