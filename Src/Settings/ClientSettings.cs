﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using CxClient.Enums;

namespace CxClient.Settings
{
    public static class ClientSettings
    {
        /// <summary>
        /// Less information in Debug screen
        /// </summary>
        public static bool ReducedDebugInfo
        {
            get;
            set;
        } = false;

        /// <summary>
        /// 
        /// </summary>
        public static bool FailOnUnknownPacket
        {
            get;
            set;
        } = false;

        /// <summary>
        /// Display type of all main menu screens
        /// </summary>
        /// <value>The current menu.</value>
        public static MenuType CurrentMenu
        {
            get;
            set;
        }

        /// <summary>
        /// Render distance in chunks
        /// </summary>
        public static byte RenderDistnce
        {
            get;
            set;
        } = 6;

        /// <summary>
        /// Character's main hand
        /// </summary>
        public static MainHand MainHand
        {
            get;
            set;
        } = MainHand.Right;

        /// <summary>
        /// Stored users with their auth tokens.
        /// </summary>
        /// <value>The stored users.</value>
        public static List<UserInfo> StoredUsers
        {
            get;
        } = new List<UserInfo>();

        #region Messages and Commands

        /// <summary>
        /// Message length limitation.<br>
        /// Required by Minecraft protocol.
        /// </summary>
        public static int MaximumMessageLength
        {
            get;
            set;
        } = 256;

        /// <summary>
        /// Command prefix character
        /// </summary>
        public static char CommandCharacter
        {
            get;
            set;
        } = '/';

        #endregion

        #region Resourcepacks

        /// <summary>
        /// Directory where are stored client's resourcepacks.
        /// </summary>
        public static string ResourcePackFolder
        {
            get;
            internal set;
        } = "resourcepacks";

        /// <summary>
        /// Directory where are stored server's (downloaded) resourcepacks.
        /// </summary>
        public static string ServerResourcePackFolder
        {
            get;
            internal set;
        } = "server_resourcepacks";

        /// <summary>
        /// Regex for resourcepacks.<br>
        /// Accepts both ZIP files and folders (possible slash).
        /// </summary>
        public static string ResourcePackRegex
        {
            get;
            internal set;
        } = "^([a-zA-Z0-9_]+\\.zip)|([a-zA-Z0-9_]+[/]?)$";

        #endregion

        #region Language

        /// <summary>
        /// Main directory where are store language files.
        /// </summary>
        public static string LanguageDirectory
        {
            get;
        } = "lang";

        internal static string _language = Translator.Languages.AmericanEnglish;
        /// <summary>
        /// Current language.<br>
        /// Restart / reconnect is recommended.
        /// </summary>
        public static string Language
        {
            get
            {
                return _language;
            }
            set
            {
                Translator.ChangeLanguage(value);
            }
        }

        #endregion

        #region Mods

        /// <summary>
        /// Directory where are stored mods.
        /// </summary>
        public static string ModsDirectory
        {
            get;
        } = "mods";

        /// <summary>
        /// Regex for loading mods.<br>
        /// Accepts DLL and SO files.
        /// </summary>
        /// <value>The mods regex.</value>
        public static string ModsRegex
        {
            get;
        } = "*.dll|*.so$";

        #endregion

        #region Scripts

        /// <summary>
        /// Directory where are stored scripts for ScriptingAPI.
        /// </summary>
        public static string ScriptsDirectory
        {
            get;
        } = "scripts";

        /// <summary>
        /// Regex for loading scripts.<br>
        /// Currently accepts only LUA files.
        /// </summary>
        public static string ScriptsRegex
        {
            get;
            internal set;
        } = "*.lua$";

        #endregion

        #region Version

        public static string Version
        {
            get;
        } = "Alpha 0.0.1";

        /// <summary>
        /// Game branch.<br>
        /// Used in window title and send to server after connecting (if enabled).
        /// </summary>
        public static string Branch
        {
            get;
        } = "CxClient";

        /// <summary>
        /// Should client send it's branch to server?
        /// </summary>
        public static bool SendBranchToServer
        {
            get;
            set;
        } = true;

        #endregion

        #region Chat

        /// <summary>
        /// Changes how does client's chat work.
        /// </summary>
        public static ChatMode ChatVisibility
        {
            get;
            set;
        }

        /// <summary>
        /// Global enable/disable of chat.
        /// </summary>
        public static bool ChatEnabled
        {
            get;
            set;
        } = true;

        /// <summary>
        /// Transparency of chat background.
        /// </summary>
        public static float ChatBackgroundTransparency
        {
            get;
            set;
        } = 0.5f;

        /// <summary>
        /// Parse links in chat.<br>
        /// Some servers may parse them for you.
        /// </summary>
        public static bool ParseChatLinks
        {
            get;
            set;
        } = true;

        /// <summary>
        /// Warn before opening chat links.
        /// </summary>
        public static bool WarnOnChatLinks
        {
            get;
            set;
        } = true;

        #endregion

        #region Graphics

        /// <summary>
        /// In which mode to start the game (may not be supported by Render Engine).
        /// </summary>
        public static WindowMode WindowMode
        {
            get;
            set;
        } = WindowMode.WindowWithBorder;

        public static float Brightness
        {
            get;
            set;
        } = 1f;

        public static ShadowMode ShadowMode
        {
            get;
            set;
        } = ShadowMode.Simple;

        public static bool EntityShadows
        {
            get;
            set;
        } = true;

        /// <summary>
        /// Maximum FPS (FPS lock).<br>
        /// 0 = unlimited<br>
        /// Recommended 30+ FPS (better 60+).
        /// </summary>
        public static ushort MaxFPS
        {
            get;
            set;
        } = 60;

        /// <summary>
        /// Should clouds be renderer?
        /// </summary>
        public static bool CloudsEnabled
        {
            get;
            set;
        } = true;

        /// <summary>
        /// Render mode for clouds.
        /// </summary>
        public static CloudsMode CloudsMode
        {
            get;
            set;
        } = CloudsMode.Simple3D;

        /// <summary>
        /// Color of clouds.<br>
        /// May be a bit different for each cloud (based of renderer).<br>
        /// Bright colors are recommended.
        /// </summary>
        public static Color CloudsColor
        {
            get;
            set;
        } = Color.White;

        #endregion

        public static void LoadConfig(string path)
        {
            //TODO load client settings
        }

        public static bool SaveConfig(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
                if (File.Exists(path))
                    return false;
            }

            using (StreamWriter writer = new StreamWriter(File.Open(path, FileMode.CreateNew), System.Text.Encoding.UTF8))
            {
                writer.AutoFlush = false;

                //
                {
                    writer.Write("XX=");
                    writer.WriteLine("xx");
                }

                writer.Flush();
            }

            return true;
        }
    }
}